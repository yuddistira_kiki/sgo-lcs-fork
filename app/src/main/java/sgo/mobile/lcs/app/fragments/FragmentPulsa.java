package sgo.mobile.lcs.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.beans.CommunityBean;
import sgo.mobile.lcs.app.beans.DenomBean;
import sgo.mobile.lcs.app.ui.dialog.DefinedDialog;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

public class FragmentPulsa extends Fragment {

    private ProgressDialog pDialog;

    String Result;
    String Name, NumberOfPeople, Date, Time, Phone, Date_n_Time;
    String COMM_ID, MASTER_PRODUCT_ID, PRODUCT_ID, PRODUCT_PRICE, MOBILE_PHONE_NO;

    String[] Operator;

    static Spinner cbo_community = null;
    static Spinner cbo_denom = null;

    EditText txtMobileNumber;
    Button btnSend;
    Button btnCancel;

    private static final String TAG_HEADER 		 = "header";

    private static final String TAG_RQ_UUID 	 = "rq_uuid";
    private static final String TAG_RS_DATETIME  = "rs_datetime";
    private static final String TAG_RECEIVER_ID  = "receiver_id";
    private static final String TAG_SENDER_ID 	 = "sender_id";
    private static final String TAG_SERVICE_NAME = "service_name";

    private static final String TAG_BODY 		 = "body";

    private static final String TAG_ERROR_CODE 	 = "error_code";
    private static final String TAG_ERROR_MSG 	 = "error_msg";
    private static final String TAG_RECONCILE_ID = "reconcile_id";
    private static final String TAG_RECONCILE_DATETIME = "reconcile_datetime";
    private static final String TAG_DESC 			   = "desc";
    private static final String TAG_DESC2 			   = "desc2";
    private static final String TAG_REMARK 			   = "remark1";

    private static final String TAG_PAYMENT_ID 		   = "payment_id";
    private static final String TAG_REL_INQUIRY_TRX    = "rel_inquiry_trx";
    private static final String TAG_COMM_ID 		   = "comm_id";
    private static final String TAG_MASTER_PRODUCT_ID  = "master_product_id";
    private static final String TAG_PRODUCT_ID 		   = "product_id";
    private static final String TAG_PRODUCT_PRICE 	   = "product_price";
    private static final String TAG_MOBILE_PHONE_NO    = "mobile_phone_no";
    private static final String TAG_ERR_CODE 		   = "err_code";
    private static final String TAG_REQ_ERR_MSG 	   = "req_err_msg";
    private static final String TAG_REF 			   = "ref";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pulsa_1, container, false);

        cbo_community = (Spinner)view.findViewById(R.id.cbo_community);
        ArrayAdapter<CommunityBean> communityArrayAdapter = new ArrayAdapter<CommunityBean>(this.getActivity(),
                R.layout.spinner_layout, new CommunityBean[] {
                new CommunityBean( "314", "TOPUP BBS PULSA" )
        });
        cbo_community.setAdapter(communityArrayAdapter);

        cbo_denom = (Spinner)view.findViewById(R.id.cbo_denom);
        ArrayAdapter<DenomBean> denomArrayAdapter = new ArrayAdapter<DenomBean>(this.getActivity(),
                R.layout.spinner_layout, new DenomBean[] {

                new DenomBean( "MS5", "5000", "T- SEL REG 5000" ),
                new DenomBean( "MS10", "10000", "T- SEL REG 10000" ),
                new DenomBean( "MS20", "20000", "T- SEL REG 20000" ),
                new DenomBean( "MS25", "25000", "T- SEL REG 25000" ),
                new DenomBean( "MS50", "50000", "T- SEL REG 50000" ),
                new DenomBean( "MS100", "100000", "T- SEL REG 100000" ),

                new DenomBean( "MN1", "1000", "AXIS REG 1000" ),
                new DenomBean( "MN5", "5000", "AXIS REG 5000" ),
                new DenomBean( "MN10", "10000", "AXIS REG 10000" ),
                new DenomBean( "MN20", "20000", "AXIS REG 20000" ),
                new DenomBean( "MN25", "25000", "AXIS REG 25000" ),
                new DenomBean( "MN50", "50000", "AXIS REG 50000" ),
                new DenomBean( "MN100", "100000", "AXIS REG 100000" ),

                new DenomBean( "ME1", "1000", "ESIA REG 1000" ),
                new DenomBean( "ME5", "5000", "ESIA REG 5000" ),
                new DenomBean( "ME10", "10000", "ESIA REG 10000" ),
                new DenomBean( "ME11", "11000", "ESIA REG 11000" ),
                new DenomBean( "ME15", "15000", "ESIA REG 15000" ),
                new DenomBean( "ME20", "20000", "ESIA REG 20000" ),
                new DenomBean( "ME25", "25000", "ESIA REG 25000" ),
                new DenomBean( "ME50", "50000", "ESIA REG 50000" ),
                new DenomBean( "ME100", "100000", "ESIA REG 100000" ),

                new DenomBean( "MT5", "5000", "FLEXI REG 5000" ),
                new DenomBean( "MT10", "10000", "FLEXI REG 10000" ),
                new DenomBean( "MT20", "20000", "FLEXI REG 20000" ),
                new DenomBean( "MT25", "25000", "FLEXI REG 25000" ),
                new DenomBean( "MT50" , "50000", "FLEXI REG 50000" ),
                new DenomBean( "MT100", "100000", "FLEXI REG 100000" ),
                new DenomBean( "MT150", "150000", "FLEXI REG 150000" ),
                new DenomBean( "MT250", "250000", "FLEXI REG 250000" ),

                new DenomBean( "MI5", "5000", "INDOSAT REG 5000" ),
                new DenomBean( "MIG5", "5000", "INDOSAT GPRS 5000" ),
                new DenomBean( "MIS5", "5000", "INDOSAT SMS 5000" ),
                new DenomBean( "MIS10", "10000", "INDOSAT SMS 10000" ),
                new DenomBean( "MI10", "10000", "INDOSAT REG 10000" ),
                new DenomBean( "MIG10", "10000", "INDOSAT GPRS 10000" ),
                new DenomBean( "MI12", "12000", "INDOSAT REG 12000" ),
                new DenomBean( "MI25", "25000", "INDOSAT REG 25000" ),
                new DenomBean( "MIS25", "25000", "INDOSAT SMS 25000" ),
                new DenomBean( "MIG25", "25000", "INDOSAT GPRS 25000" ),
                new DenomBean( "MI50", "50000", "INDOSAT REG 50000" ),
                new DenomBean( "MI100", "100000", "INDOSAT REG 100000" ),

                new DenomBean( "MF5", "5000", "SMARTFREN REG 5000" ),
                new DenomBean( "MF10", "10000", "SMARTFREN REG 10000" ),
                new DenomBean( "MF20", "20000", "SMARTFREN REG 20000" ),
                new DenomBean( "MF25", "25000", "SMARTFREN REG 25000" ),
                new DenomBean( "MF30", "30000", "SMARTFREN REG 30000" ),
                new DenomBean( "MF50", "50000", "SMARTFREN REG 50000" ),
                new DenomBean( "MF75", "75000", "SMARTFREN REG 75000" ),
                new DenomBean( "MF100", "100000", "SMARTFREN REG 100000" ),
                new DenomBean( "MF150", "150000", "SMARTFREN REG 150000" ),
                new DenomBean( "MF200", "200000", "SMARTFREN REG 200000" ),
                new DenomBean( "MF225", "225000", "SMARTFREN REG 225000" ),
                new DenomBean( "MF300", "300000", "SMARTFREN REG 300000" ),
                new DenomBean( "MF500", "500000", "SMARTFREN REG 500000" ),

                new DenomBean( "MH1", "1000", "THREE REG 1000" ),
                new DenomBean( "MH5", "5000", "THREE REG 5000" ),
                new DenomBean( "MH10", "10000", "THREE REG 10000" ),
                new DenomBean( "MH20", "20000", "THREE REG 20000" ),
                new DenomBean( "MH30", "30000", "THREE REG 30000" ),
                new DenomBean( "MH50", "50000", "THREE REG 50000" ),
                new DenomBean( "MH100", "100000", "THREE REG 100000" ),

                new DenomBean( "MG5M", "35000", "THREE INTERNET 500 MB" ),
                new DenomBean( "MG1G", "50000", "THREE INTERNET 1 GB" ),
                new DenomBean( "MG2G", "75000", "THREE INTERNET 2 GB" ),
                new DenomBean( "MG5G", "125000", "THREE INTERNET 5 GB" ),

                new DenomBean( "MR5", "5000", "XL REG 5000" ),
                new DenomBean( "MR10", "10000", "XL REG 10000" ),
                new DenomBean( "MR25", "25000", "XL REG 25000" ),
                new DenomBean( "MR50", "50000", "XL REG 50000" ),
                new DenomBean( "MR100", "100000", "XL REG 100000" )
        });
        cbo_denom.setAdapter(denomArrayAdapter);

        cbo_denom = (Spinner)view.findViewById(R.id.cbo_denom);

        btnSend   = (Button) view.findViewById(R.id.btnSend);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);
        txtMobileNumber = (EditText) view.findViewById(R.id.txt_mobile_number);

        btnSend.setOnClickListener(new View.OnClickListener() {


            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                CommunityBean commCode = (CommunityBean)cbo_community.getSelectedItem();
                DenomBean denomValue = (DenomBean)cbo_denom.getSelectedItem();

                COMM_ID            = String.valueOf(commCode.id);
                PRODUCT_ID         = String.valueOf(denomValue.id);
                PRODUCT_PRICE      = String.valueOf(denomValue.price);
                MOBILE_PHONE_NO    = txtMobileNumber.getText().toString();

                if(MOBILE_PHONE_NO.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing purchase...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("COMM_ID", COMM_ID);
                    params.put("PRODUCT_ID", PRODUCT_ID);
                    params.put("PRODUCT_PRICE", PRODUCT_PRICE);
                    params.put("MOBILE_PHONE_NO", MOBILE_PHONE_NO);

                    Log.d("params", params.toString());
                    client.post(AplConstants.PulsaMobileAPI, params, new AsyncHttpResponseHandler(){
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {

                                JSONObject object         = new JSONObject(content);
                                JSONObject header_param_data = object.getJSONObject("header");
                                String rq_uuid               = header_param_data.getString("rq_uuid");
                                String rs_datetime           = header_param_data.getString("rs_datetime");
                                String receiver_id           = header_param_data.getString("receiver_id");
                                String sender_id             = header_param_data.getString("sender_id");
                                String service_name          = header_param_data.getString("service_name");

                                JSONObject body_param_data   = object.getJSONObject("body");
                                String error_code            = body_param_data.getString("error_code");
                                String error_msg             = body_param_data.getString("error_msg");
                                String reconcile_id          = body_param_data.getString("reconcile_id");
                                String reconcile_datetime    = body_param_data.getString("reconcile_datetime");
                                String desc                  = body_param_data.getString("desc");
                                String desc2                 = body_param_data.getString("desc2");

                                String payment_id            = object.getString("payment_id");
                                String rel_inquiry_trx       = object.getString("rel_inquiry_trx");
                                String comm_id               = object.getString("comm_id");
                                String master_product_id     = object.getString("master_product_id");
                                String product_id            = object.getString("product_id");
                                String product_price         = object.getString("product_price");
                                String mobile_phone_no       = object.getString("mobile_phone_no");
                                String err_code              = object.getString("err_code");
                                String req_err_msg           = object.getString("req_err_msg");
                                String ref                   = object.getString("ref");

                                if (pDialog != null){
                                    pDialog.dismiss();
                                }

                                if( error_code.equals(AppParams.SUCCESS_CODE) ){
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Isi Pulsa");
                                    alert.setMessage("Pengisian pulsa berhasil silahkan tunggu sms notifikasi serta cek saldo pulsa anda beberapa saat lagi.");
                                    alert.setPositiveButton("OK",null);
                                    alert.show();
                                }else{
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Isi Pulsa");
                                    alert.setMessage("TopUp Pulsa : " + error_msg);
                                    alert.setPositiveButton("OK",null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });

        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };
}
