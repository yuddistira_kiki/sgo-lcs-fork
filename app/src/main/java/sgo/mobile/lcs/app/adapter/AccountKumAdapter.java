package sgo.mobile.lcs.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.beans.AccountKumBean;

/**
 * Created by thinkpad on 3/29/2016.
 */
public class AccountKumAdapter extends BaseAdapter {
    private Activity activity;
    ArrayList<AccountKumBean> data;

    public AccountKumAdapter(Activity act, ArrayList<AccountKumBean> data) {
        this.activity = act;
        this.data = data;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return data.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.list_item_account_kum, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.txtBankName    = (TextView) convertView.findViewById(R.id.acckum_bank_name);
        holder.txtAcctNo = (TextView) convertView.findViewById(R.id.acckum_acc_no);
        holder.txtAcctName = (TextView) convertView.findViewById(R.id.acckum_acc_name);

        holder.txtBankName.setText(data.get(position).getBank_name());
        holder.txtAcctNo.setText(data.get(position).getAcct_no());
        holder.txtAcctName.setText(data.get(position).getAcct_name());

        return convertView;
    }

    static class ViewHolder {
        TextView txtBankName, txtAcctNo, txtAcctName;
    }
}
