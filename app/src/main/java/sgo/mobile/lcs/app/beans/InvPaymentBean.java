package sgo.mobile.lcs.app.beans;

public class InvPaymentBean {
    public String doc_no = "";
    public String doc_id = "";
    public String amount = "";
    public String remain_amount = "";
    public String ccy = "";
    public String doc_desc = "";

    public InvPaymentBean(String doc_no, String doc_id, String amount, String remain_amount, String ccy, String doc_desc) {
        this.doc_no = doc_no;
        this.doc_id = doc_id;
        this.amount = amount;
        this.remain_amount = remain_amount;
        this.ccy = ccy;
        this.doc_desc = doc_desc;
    }

    public String getDoc_no() {
        return doc_no;
    }

    public void setDoc_no(String doc_no) {
        this.doc_no = doc_no;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getRemain_amount() {
        return remain_amount;
    }

    public void setRemain_amount(String remain_amount) {
        this.remain_amount = remain_amount;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getDoc_desc() {
        return doc_desc;
    }

    public void setDoc_desc(String doc_desc) {
        this.doc_desc = doc_desc;
    }

    @Override
    public String toString() {
        return "InvPaymentBean{" +
                "doc_no='" + doc_no + '\'' +
                ", doc_id='" + doc_id + '\'' +
                '}';
    }
}
