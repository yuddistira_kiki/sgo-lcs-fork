package sgo.mobile.lcs.app.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.AppHelper;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.adapter.CommunityInvAdapter;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentCommunityInvPayment extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;

    CommunityInvAdapter CommunityInvAdapter;

    // create arraylist variables to store data from server
    public static ArrayList<String> id       = new ArrayList<String>();
    public static ArrayList<String> code     = new ArrayList<String>();
    public static ArrayList<String> name     = new ArrayList<String>();
    public static ArrayList<String> sales_alias         = new ArrayList<String>();
    public static ArrayList<String> buss_scheme_code    = new ArrayList<String>();

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_community_list, container, false);
        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);

        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText("Komunitas AR-Collection");


        CommunityInvAdapter = new CommunityInvAdapter(getActivity());
        parseJSONData();

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {

                Fragment newFragment = null;
                newFragment = new FragmentMemberInvPayment();
                Bundle args = new Bundle();
                args.putString("comm_id", id.get(position));
                args.putString("comm_name", name.get(position));
                args.putString("comm_code", code.get(position));
                args.putString("sales_alias", sales_alias.get(position));
                args.putString("buss_scheme_code",buss_scheme_code.get(position));

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();

        String param_sales_id = AppHelper.getUserId(getActivity());
        params.put("sales_id", param_sales_id);

        Log.d("params", params.toString());
        client.post(AplConstants.CommunityInvMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code         = json.getString("error_code");
                    String error_message      = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        JSONArray data = json.getJSONArray("community"); // this is the "items: [ ] part
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            id.add(object.getString("comm_id"));
                            code.add(object.getString("comm_code"));
                            name.add(object.getString("comm_name"));
                            sales_alias.add(object.getString("sales_alias"));
                            buss_scheme_code.add(object.getString("buss_scheme_code"));
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(8);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(id.size() > 0){
                            listMenu.setVisibility(0);
                            listMenu.setAdapter(CommunityInvAdapter);
                            lbl_header.setVisibility(0);
                        }else{
                            txtAlert.setVisibility(0);
                        }

                    }else{
                        prgLoading.setVisibility(8);
                        txtAlert.setVisibility(0);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            ;

            public void onFailure(Throwable error, String content) {
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

            ;
        });
    }

    // clear arraylist variables before used
    void clearData(){
        id.clear();
        code.clear();
        name.clear();
        sales_alias.clear();
        buss_scheme_code.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}