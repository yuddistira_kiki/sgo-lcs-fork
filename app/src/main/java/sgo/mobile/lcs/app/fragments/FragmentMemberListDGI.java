package sgo.mobile.lcs.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.securepreferences.SecurePreferences;

import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.AppHelper;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.adapter.MemberDGIAdapter;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentMemberListDGI extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    SecurePreferences prefs;
    Activity activity;

    public String comm_id, comm_name, comm_code, sales_alias, buss_scheme_code;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;

    MemberDGIAdapter memberDGIAdapter;

    // create arraylist variables to store data from server
    public static ArrayList<String> id   = new ArrayList<String>();
    public static ArrayList<String> code = new ArrayList<String>();
    public static ArrayList<String> name = new ArrayList<String>();
    public static ArrayList<String> member_cust_id = new ArrayList<String>();

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    boolean isKUM = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_member_list, container, false);
        activity = getActivity();
        prefs = new SecurePreferences(activity);

        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        lbl_header = (TextView) view.findViewById(R.id.label_header);

        Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        sales_alias           = bundle.getString("sales_alias");
        buss_scheme_code      = bundle.getString("buss_scheme_code");

        if(bundle.containsKey(AppParams.KUM))
            isKUM = bundle.getBoolean(AppParams.KUM, false);

        memberDGIAdapter = new MemberDGIAdapter(getActivity());
        parseJSONData();

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {

                Fragment newFragment = null;
                if(isKUM) {
                    newFragment = new FragmentListAccKum();
                    Bundle args = new Bundle();
                    args.putString(AppParams.MEMBER_CUST_ID, member_cust_id.get(position));
                    newFragment.setArguments(args);
                    switchFragment(newFragment);
                }
                else {
                    newFragment = new FragmentInvoiceListDGI();
                    Bundle args = new Bundle();
                    args.putString("member_id", id.get(position));
                    args.putString("member_code", code.get(position));
                    args.putString("member_name", name.get(position));
                    args.putString("session_id_param", "null");

                    args.putString("comm_id", comm_id);
                    args.putString("comm_name", comm_name);
                    args.putString("comm_code", comm_code);
                    args.putString("sales_alias", sales_alias);
                    args.putString("buss_scheme_code", buss_scheme_code);

                    newFragment.setArguments(args);
                    switchFragment(newFragment);
                }
            }
        });


        return view;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
        RequestParams params = new RequestParams();
        String param_sales_id = AppHelper.getUserId(activity);
        params.put("comm_id", comm_id);
        params.put("sales_id", param_sales_id);

        Log.d("params", params.toString());
        client.post(AplConstants.DiMemberMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code         = json.getString("error_code");
                    String error_message      = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        JSONArray data = json.getJSONArray("member_data"); // this is the "items: [ ] part
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            id.add(object.getString("member_id"));
                            code.add(object.getString("member_code"));
                            name.add(object.getString("member_name"));
                            member_cust_id.add(object.getString("member_cust_id"));
                        }

                        String bankData = json.getString(AppParams.BANK_DATA);
                        String bankEspay = json.getString(AppParams.BANK_ESPAY);

                        String comm_id = json.getString(AppParams.COMM_ID);
                        String comm_name = json.getString(AppParams.COMM_NAME);
                        String comm_code = json.getString(AppParams.COMM_CODE);
                        String api_key = json.getString(AppParams.API_KEY);

                        SecurePreferences.Editor mEditor = prefs.edit();
                        mEditor.putString(AppParams.BANK_DATA, bankData);
                        mEditor.putString(AppParams.BANK_ESPAY, bankEspay);
//                        mEditor.putString(AppParams.COMM_ID, comm_id);
//                        mEditor.putString(AppParams.COMM_NAME, comm_name);
//                        mEditor.putString(AppParams.COMM_CODE, comm_code);
                        mEditor.putString(AppParams.API_KEY, api_key);
                        mEditor.apply();

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(8);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(id.size() > 0){
                            listMenu.setVisibility(0);
                            listMenu.setAdapter(memberDGIAdapter);
                            lbl_header.setVisibility(0);
                        }else{
                            txtAlert.setVisibility(0);
                        }

                    }else{
                        prgLoading.setVisibility(8);
                        txtAlert.setVisibility(0);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            ;

            public void onFailure(Throwable error, String content) {
                Toast.makeText(activity, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

            ;
        });
    }

    // clear arraylist variables before used
    void clearData(){
        id.clear();
        code.clear();
        name.clear();
        member_cust_id.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}