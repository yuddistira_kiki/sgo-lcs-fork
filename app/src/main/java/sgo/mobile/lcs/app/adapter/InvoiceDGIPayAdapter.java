package sgo.mobile.lcs.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.fragments.FragmentInvoicePayDGI;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;

public class InvoiceDGIPayAdapter extends BaseAdapter {
    private Activity activity;

    public InvoiceDGIPayAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentInvoicePayDGI.doc_no_pay.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_invdgipay_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        String InputAmount = FragmentInvoicePayDGI.input_amount_pay.get(position);
        long remain = Long.parseLong(FragmentInvoicePayDGI.remain_amount_pay.get(position));
        //int remain_amount = remain - Integer.parseInt(InputAmount);
        long hold   = Long.parseLong(FragmentInvoicePayDGI.hold_amount_pay.get(position));
        long remain_amount = remain - hold;
        remain_amount = Math.max(0, remain_amount);

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText("Invoice " + FragmentInvoicePayDGI.doc_no_pay.get(position));
        holder.txtSubText.setText( "Sisa : " +  FormatCurrency.getRupiahFormat(Long.toString(remain_amount)));

        if (InputAmount != null && !InputAmount.equals("") && !InputAmount.equals("null") && Long.parseLong(InputAmount) > 0){
            holder.txtSubText2.setVisibility(View.VISIBLE);
            holder.txtSubText2.setText( "Bayar : " +  FormatCurrency.getRupiahFormat(InputAmount));
        }else{
            holder.txtSubText2.setVisibility(View.GONE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtSubText2;
    }
}
