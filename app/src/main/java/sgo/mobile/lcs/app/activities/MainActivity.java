package sgo.mobile.lcs.app.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;


import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.fragments.MainListFragment;

public class MainActivity extends BaseMainActivity implements MainListFragment.OnFragmentUpdateListener {

    FragmentManager fm;
    private static final String TAG = "MainActivity";
    private Fragment mContent;
    public static final int ACTIVITY_RESULT = 1;
    public static final int REQUEST_EXIT = 0 ;
    public static final int RESULT_ERROR = -1 ;//untuk Result Code dari child activity ke parent activity kalau error (aplikasi exit)


    @Override
    public void onFragmentUpdate(int position, boolean forceupdate) {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_main);

        Bundle extras = getIntent().getExtras();
        if(extras != null) {
            Intent i = getIntent();
        }

        fm = getSupportFragmentManager();

        MainListFragment fragment = (MainListFragment)fm.findFragmentByTag("main_fragment");
        if (savedInstanceState == null || fragment == null) {
            fragment = MainListFragment.newInstance();
            FragmentTransaction ft = fm.beginTransaction();
            ft.add(android.R.id.content, fragment, "main_fragment");
            ft.commit();
        }

        actionBarSetup();
    }

    public void switchContent(Fragment fragment) {
        mContent = fragment;
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment).commit();
        getSlidingMenu().showContent();
    }

    public void SignOut(View view)
    {
        AlertDialog.Builder alertbox=new AlertDialog.Builder(this);
        alertbox.setTitle("Peringatan");
        alertbox.setMessage("Keluar dari aplikasi?");
        alertbox.setPositiveButton("Ya", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        Intent intent = new Intent(MainActivity.this, ActivityLogin.class);
                        startActivity(intent);
                        finish();
                    }
                });
        alertbox.setNegativeButton("Tidak", new
                DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {}
                });
        alertbox.show();
    }

    // Working for all API levels
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AlertDialog.Builder alertbox=new AlertDialog.Builder(this);
            alertbox.setTitle("Peringatan");
            alertbox.setMessage("Keluar dari aplikasi?");
            alertbox.setPositiveButton("Ya", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {
                            Intent intent = new Intent(MainActivity.this, ActivityLogin.class);
                            startActivity(intent);
                            finish();
                        }
                    });
            alertbox.setNegativeButton("Tidak", new
                    DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface arg0, int arg1) {}
                    });
            alertbox.show();
        }
        return super.onKeyDown(keyCode, event);
    }

    public void switchActivity(Intent mIntent, int activity_type) {
        switch (activity_type){
            case ACTIVITY_RESULT:
                startActivityForResult(mIntent,REQUEST_EXIT);
                break;
            case 2:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("isi request code", String.valueOf(requestCode));
        Log.d("isi result Code", String.valueOf(resultCode));

        if (requestCode == REQUEST_EXIT) {
            if (resultCode == RESULT_ERROR) {
                this.finish();
            }
        }
    }
}
