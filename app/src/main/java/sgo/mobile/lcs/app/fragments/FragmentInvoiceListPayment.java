package sgo.mobile.lcs.app.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.AppHelper;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.adapter.BankAdapter;
import sgo.mobile.lcs.app.adapter.InvoicePaymentAdapter;
import sgo.mobile.lcs.app.beans.BankBean;
import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class FragmentInvoiceListPayment extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    Activity activity;

    private String[] bank_code_arr, bank_name_arr, ccy_id_arr, product_code_arr, member_phone_arr, benef_acct_no_arr, benef_acct_name_arr, tx_fee_arr, fee_acct_no_arr, seller_fee_arr, charges_acct_no_arr, buyer_fee_arr;
    private static Spinner cbo_bank      = null;
    private ArrayList<BankBean> BankList = new ArrayList<BankBean>();

    public String member_id, member_code, member_name, session_id_param;
    public String total_pay;
    public String session_id_var;
    public String comm_id, comm_name, comm_code, sales_alias, buss_scheme_code;
    public static String bank_code, bank_name, ccy_id, product_code, seller_fee, buyer_fee, tx_fee, member_phone, benef_acct_no, benef_acct_name, charges_acct_no, fee_acct_no;

    // declare view objects
    ListView listMenu;
    ProgressBar prgLoading;
    TextView txtAlert;
    TextView lbl_header;
    TableLayout tabel_footer;
    TextView lbl_total_pay_amount;

    InvoicePaymentAdapter invoicePaymentAdapter;

    // create arraylist variables to store data from server
    private String partial_payment = "";
    private String flag_continue   = "";

    public static ArrayList<String> doc_no = new ArrayList<String>();
    public static ArrayList<String> doc_id = new ArrayList<String>();
    public static ArrayList<String> amount = new ArrayList<String>();
    public static ArrayList<String> remain_amount = new ArrayList<String>();
    public static ArrayList<String> input_amount = new ArrayList<String>();
    public static ArrayList<String> ccy = new ArrayList<String>();
    public static ArrayList<String> doc_desc = new ArrayList<String>();
    public static ArrayList<String> session_id = new ArrayList<String>();

    Button btnDone;
    Button btnCancel;
    Button btnBack;

    // create price format
    DecimalFormat formatData = new DecimalFormat("#.##");

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.fragment_invpayment_list, container, false);
        activity = getActivity();

        txtAlert = (TextView) view.findViewById(R.id.txtAlert);
        prgLoading = (ProgressBar) view.findViewById(R.id.prgLoading);
        listMenu   = (ListView) view.findViewById(R.id.listMenu);
        tabel_footer = (TableLayout) view.findViewById(R.id.tabel_footer);
        lbl_total_pay_amount = (TextView) view.findViewById(R.id.lbl_total_pay_amount);


        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);
        btnBack                = (Button) view.findViewById(R.id.btnBack);

        cbo_bank              = (Spinner)view.findViewById(R.id.cbo_bank);

        Bundle bundle         = this.getArguments();
        member_id             = bundle.getString("member_id");
        member_code           = bundle.getString("member_code");
        member_name           = bundle.getString("member_name");
        session_id_param      = bundle.getString("session_id_param");
        session_id_param      = (session_id_param.equalsIgnoreCase("null")) ? "" : session_id_param;

        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        sales_alias           = bundle.getString("sales_alias");
        buss_scheme_code      = bundle.getString("buss_scheme_code");


        lbl_header = (TextView) view.findViewById(R.id.label_header);
        lbl_header.setText(member_name);

        invoicePaymentAdapter = new InvoicePaymentAdapter(getActivity());
        parseJSONData();

        // event listener to handle list when clicked
        listMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {
                BankBean bankSelected  = (BankBean) cbo_bank.getSelectedItem();
                bank_code              = String.valueOf(bankSelected.bank_code);
                bank_name              = String.valueOf(bankSelected.bank_name);
                product_code           = String.valueOf(bankSelected.product_code);
                ccy_id                 = String.valueOf(bankSelected.ccy_id);
                seller_fee             = String.valueOf(bankSelected.seller_fee);
                buyer_fee              = String.valueOf(bankSelected.buyer_fee);
                tx_fee                 = String.valueOf(bankSelected.tx_fee);
                member_phone           = String.valueOf(bankSelected.member_phone);
                benef_acct_no          = String.valueOf(bankSelected.benef_acct_no);
                benef_acct_name        = String.valueOf(bankSelected.benef_acct_name);
                charges_acct_no        = String.valueOf(bankSelected.charges_acct_no);
                fee_acct_no            = String.valueOf(bankSelected.fee_acct_no);

                Fragment newFragment = null;
                newFragment = new FragmentInvoicePayment();
                Bundle args = new Bundle();
                args.putString("doc_no", doc_no.get(position));
                args.putString("doc_id", doc_id.get(position));
                args.putString("amount", amount.get(position));
                args.putString("remain_amount", remain_amount.get(position));
                args.putString("input_amount", input_amount.get(position));
                args.putString("ccy", ccy.get(position));
                args.putString("doc_desc", doc_desc.get(position));
                args.putString("partial_payment", partial_payment);
                args.putString("session_id", session_id.get(position));
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);


                args.putString("bank_code", bank_code);
                args.putString("bank_name", bank_name);
                args.putString("product_code", product_code);
                args.putString("ccy_id", ccy_id);
                args.putString("seller_fee", seller_fee);
                args.putString("buyer_fee", buyer_fee);
                args.putString("tx_fee", tx_fee);
                args.putString("member_phone", member_phone);
                args.putString("benef_acct_no", benef_acct_no);
                args.putString("benef_acct_name", benef_acct_name);
                args.putString("charges_acct_no", charges_acct_no);
                args.putString("fee_acct_no", fee_acct_no);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                if(total_pay != null && !total_pay.equals("") && !total_pay.equals("null") && Double.parseDouble(total_pay) > 0)
                {

                    BankBean bankSelected  = (BankBean) cbo_bank.getSelectedItem();
                    bank_code              = String.valueOf(bankSelected.bank_code);
                    bank_name              = String.valueOf(bankSelected.bank_name);
                    product_code           = String.valueOf(bankSelected.product_code);
                    ccy_id                 = String.valueOf(bankSelected.ccy_id);
                    seller_fee             = String.valueOf(bankSelected.seller_fee);
                    buyer_fee              = String.valueOf(bankSelected.buyer_fee);
                    tx_fee                 = String.valueOf(bankSelected.tx_fee);
                    member_phone           = String.valueOf(bankSelected.member_phone);
                    benef_acct_no          = String.valueOf(bankSelected.benef_acct_no);
                    benef_acct_name        = String.valueOf(bankSelected.benef_acct_name);
                    charges_acct_no        = String.valueOf(bankSelected.charges_acct_no);
                    fee_acct_no            = String.valueOf(bankSelected.fee_acct_no);

                    Fragment newFragment = null;
                    newFragment = new FragmentInvoicePayPayment();
                    Bundle args = new Bundle();
                    args.putString("member_id", member_id);
                    args.putString("member_code", member_code);
                    args.putString("member_name", member_name);
                    args.putString("session_id_param", session_id_var);
                    args.putString("ccy_id", ccy_id);

                    args.putString("comm_id", comm_id);
                    args.putString("comm_name", comm_name);
                    args.putString("comm_code", comm_code);
                    args.putString("sales_alias", sales_alias);
                    args.putString("buss_scheme_code", buss_scheme_code);

                    args.putString("bank_code", bank_code);
                    args.putString("bank_name", bank_name);
                    args.putString("product_code", product_code);
                    args.putString("ccy_id", ccy_id);
                    args.putString("seller_fee", seller_fee);
                    args.putString("buyer_fee", buyer_fee);
                    args.putString("tx_fee", tx_fee);
                    args.putString("member_phone", member_phone);
                    args.putString("benef_acct_no", benef_acct_no);
                    args.putString("benef_acct_name", benef_acct_name);
                    args.putString("charges_acct_no", charges_acct_no);
                    args.putString("fee_acct_no", fee_acct_no);

                    args.putString("flag_continue", flag_continue);
                    args.putString("partial_payment", partial_payment);

                    newFragment.setArguments(args);
                    switchFragment(newFragment);
                }else{
                    Toast.makeText(getActivity(), "Checkout tidak dapat dilakukan bila belum ada item yang diisi!.", Toast.LENGTH_SHORT).show();
                }
            }
        });


        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentInvoiceListPayment();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", "null");

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                Fragment newFragment = null;
                newFragment = new FragmentMemberInvPayment();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void parseJSONData(){
        clearData();
        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
        RequestParams params = new RequestParams();

        String param_sales_id = AppHelper.getUserId(activity);
        params.put("member_id", member_id);
        params.put("sales_id", param_sales_id);
        params.put("session_id", session_id_param);

        Log.d("params", params.toString());
        client.post(AplConstants.InvListMobileAPI, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code         = json.getString("error_code");
                    String error_message      = json.getString("error_message");

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {

                        JSONArray array = json.getJSONArray("bank_data");

                        int len = array.length();
                        bank_code_arr = new String[len];
                        bank_name_arr = new String[len];
                        ccy_id_arr = new String[len];
                        product_code_arr = new String[len];
                        seller_fee_arr = new String[len];
                        buyer_fee_arr = new String[len];
                        tx_fee_arr = new String[len];
                        member_phone_arr = new String[len];
                        benef_acct_no_arr = new String[len];
                        benef_acct_name_arr = new String[len];
                        charges_acct_no_arr = new String[len];
                        fee_acct_no_arr = new String[len];

                        for (int i = 0; i < len; i++){
                            bank_code_arr[i]        = array.getJSONObject(i).getString("bank_code");
                            bank_name_arr[i]        = array.getJSONObject(i).getString("bank_name");
                            ccy_id_arr[i]           = array.getJSONObject(i).getString("ccy_id");
                            product_code_arr[i]     = array.getJSONObject(i).getString("product_code");
                            seller_fee_arr[i]       = array.getJSONObject(i).getString("seller_fee");
                            buyer_fee_arr[i]        = array.getJSONObject(i).getString("buyer_fee");
                            tx_fee_arr[i]           = array.getJSONObject(i).getString("tx_fee");
                            member_phone_arr[i]     = array.getJSONObject(i).getString("member_phone");
                            benef_acct_no_arr[i]    = array.getJSONObject(i).getString("benef_acct_no");
                            benef_acct_name_arr[i]  = array.getJSONObject(i).getString("benef_acct_name");
                            charges_acct_no_arr[i]  = array.getJSONObject(i).getString("charges_acct_no");
                            fee_acct_no_arr[i]      = array.getJSONObject(i).getString("fee_acct_no");

                            BankList.add(new BankBean(bank_code_arr[i], bank_name_arr[i], ccy_id_arr[i], product_code_arr[i], seller_fee_arr[i], buyer_fee_arr[i], tx_fee_arr[i], member_phone_arr[i], benef_acct_no_arr[i], benef_acct_name_arr[i], charges_acct_no_arr[i], fee_acct_no_arr[i]));
                        }

                        BankList.add(new BankBean("008", "BANK BCA", "IDR", "MANDIRISMS", "0", "0", "0", "6281514407650", "1550003436832", "PT Square Gate One", "1550003436832", ""));
                        BankList.add(new BankBean("008", "BANK BRI", "IDR", "MANDIRISMS", "0", "0", "0", "6281514407650", "1550003436832", "PT Square Gate One", "1550003436832", ""));
                        BankList.add(new BankBean("008", "BANK BNI", "IDR", "MANDIRISMS", "0", "0", "0", "6281514407650", "1550003436832", "PT Square Gate One", "1550003436832", ""));
                        BankList.add(new BankBean("008", "BANK BII", "IDR", "MANDIRISMS", "0", "0", "0", "6281514407650", "1550003436832", "PT Square Gate One", "1550003436832", ""));
                        BankList.add(new BankBean("008", "T-CASH", "IDR", "MANDIRISMS", "0", "0", "0", "6281514407650", "1550003436832", "PT Square Gate One", "1550003436832", ""));
                        BankList.add(new BankBean("008", "XL TUNAI", "IDR", "MANDIRISMS", "0", "0", "0", "6281514407650", "1550003436832", "PT Square Gate One", "1550003436832", ""));
                        BankList.add(new BankBean("008", "DOMPETKU", "IDR", "MANDIRISMS", "0", "0", "0", "6281514407650", "1550003436832", "PT Square Gate One", "1550003436832", ""));

                        BankAdapter bankAdapter = new BankAdapter(activity, android.R.layout.simple_spinner_item, BankList);
                        cbo_bank.setAdapter(bankAdapter);

                        JSONArray data = json.getJSONArray("invoice_data"); // this is the "items: [ ] part

                        partial_payment = json.getString("partial_payment");
                        flag_continue   = json.getString("flag_continue");
                        total_pay       = json.getString("total_pay");
                        session_id_var  = json.getString("session_id_var");

                        JSONArray dataFee    = json.getJSONArray("bank_data");
                        JSONObject objectFee = dataFee.getJSONObject(0);
                        ccy_id               =  objectFee.getString("ccy_id");

                        String total_payment = (total_pay != null && !total_pay.equals("") && !total_pay.equals("null") && Double.parseDouble(total_pay) > 0 )  ? FormatCurrency.getRupiahFormat(total_pay) : "";
                        lbl_total_pay_amount.setText(total_payment);

                        for (int i = 0; i < data.length(); i++) {
                            JSONObject object = data.getJSONObject(i);
                            doc_no.add(object.getString("doc_no"));
                            doc_id.add(object.getString("doc_id"));
                            amount.add(object.getString("amount"));
                            remain_amount.add(object.getString("remain_amount"));
                            ccy.add(object.getString("ccy"));
                            doc_desc.add(object.getString("doc_desc"));
                            input_amount.add(object.getString("input_amount"));
                            session_id.add(object.getString("session_id"));
                        }

                        // when finish parsing, hide progressbar
                        prgLoading.setVisibility(8);

                        // if data available show data on list
                        // otherwise, show alert text
                        if(doc_no.size() > 0){
                            listMenu.setVisibility(0);
                            listMenu.setAdapter(invoicePaymentAdapter);
                            lbl_header.setVisibility(0);
                            tabel_footer.setVisibility(0);
                        }else{
                            txtAlert.setVisibility(0);
                        }

                    }else{
                        prgLoading.setVisibility(8);
                        txtAlert.setVisibility(0);
                        txtAlert.setText(error_message);
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            };

            public void onFailure(Throwable error, String content) {
                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }

            ;
        });
    }

    // clear arraylist variables before used
    void clearData(){
        doc_no.clear();
        doc_id.clear();
        amount.clear();
        remain_amount.clear();
        ccy.clear();
        doc_desc.clear();
        input_amount.clear();
        session_id.clear();
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}