package sgo.mobile.lcs.app.beans;

public class MemberGWBean {
    public String member_id   = "";
    public String member_code = "";

    public MemberGWBean( String _id, String _code )
    {
        member_id = _id;
        member_code = _code;
    }

    public String getMember_id(){
        return member_id;
    }

    public void setMember_id(String memberId){
        this.member_id = memberId;
    }

    public String getMember_code(){
        return member_code;
    }

    public void setMember_code(String member_code){
        this.member_code = member_code;
    }

    public String toString()
    {
        return( member_code  );
    }
}