package sgo.mobile.lcs.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.ui.dialog.DefinedDialog;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

public class FragmentC2ADeposit extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;
    String community_code, requester_id, sender_no, total, account_no;

    EditText inpName;
    EditText txt_sender_number;
    EditText inpAmount;
    Button btnDone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_c2a_deposit, container, false);

        inpName               = (EditText) view.findViewById(R.id.inpName);
        txt_sender_number     = (EditText) view.findViewById(R.id.txt_sender_number);
        inpAmount             = (EditText) view.findViewById(R.id.inpAmount);

        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub

                community_code       = AplConstants.CommCodeCTA;
                requester_id         = AplConstants.MemberCodeCTA;
                sender_no            = txt_sender_number.getText().toString();
                total                = inpAmount.getText().toString();
                account_no           = inpName.getText().toString();

                if(total.equalsIgnoreCase("") || sender_no.equalsIgnoreCase("") || account_no.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing Deposit C2A...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("community_code", community_code);
                    params.put("requester_id", requester_id);
                    params.put("sender_no", sender_no);
                    params.put("amount", total);
                    params.put("total", total);
                    params.put("account_no",account_no );

                    Log.d("params", params.toString());
                    client.post(AplConstants.DepositC2AMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String rq_uuid            = object.getString("rq_uuid");
                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_msg");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                    String transaction_id     = object.getString("transaction_id");
                                    String community_code     = object.getString("community_code");
                                    String member_code        = object.getString("member_code");
                                    String sender_phone       = object.getString("sender_phone");
                                    String account_no         = object.getString("account_no");
                                    String total              = object.getString("total");
                                    String fee                = object.getString("fee");
                                    String trx_date           = object.getString("trx_date");
                                    String account_name       = object.getString("account_name");
                                    String ref                = object.getString("ref");

                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash to Account");
                                    alert.setMessage("Process Cash to Account Success, Sender phone will receive an OTP via sms for the next step");
                                    alert.setPositiveButton("OK", null);
                                    alert.show();

                                    Fragment newFragment = null;
                                    newFragment = new FragmentC2AConfirmToken();
                                    Bundle args = new Bundle();
                                    args.putString("transaction_id", transaction_id);
                                    args.putString("account_no", account_no);
                                    args.putString("account_name", account_name);
                                    newFragment.setArguments(args);
                                    switchFragment(newFragment);

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Cash to Account");
                                    alert.setMessage("Cash to Account : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
