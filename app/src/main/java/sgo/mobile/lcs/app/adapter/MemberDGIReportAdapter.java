package sgo.mobile.lcs.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.fragments.FragmentMemberReportDGI;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;

public class MemberDGIReportAdapter extends BaseAdapter {
    private Activity activity;

    public MemberDGIReportAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentMemberReportDGI.member_id.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_member_dgi_report_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Double dbl_amount = Double.parseDouble(FragmentMemberReportDGI.amount.get(position));
        int int_amount = dbl_amount.intValue();

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);

        holder.txtText.setText(FragmentMemberReportDGI.member_shop.get(position));
        holder.txtSubText.setText("Jumlah : " +  FormatCurrency.getRupiahFormat(Integer.toString(int_amount)));

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText;
    }
}
