package sgo.mobile.lcs.app.dialogs;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import sgo.mobile.lcs.R;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;

/**
 * Created by thinkpad on 1/18/2016.
 */
public class DialogInvRepaymentDesc extends DialogFragment {
    View view;

    public String tx_id, max_tx_date, amount, ccy, desc;
    public String comm_id, comm_name, comm_code;

    Button btnDone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_invrepayment_desc, container, false);
        getDialog().requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);

        btnDone               = (Button) view.findViewById(R.id.btnDone);

        final Bundle bundle   =  this.getArguments();
        tx_id                 =  bundle.getString(AppParams.TX_ID);
        max_tx_date           =  bundle.getString(AppParams.MAX_TX_DATE);
        amount                =  bundle.getString(AppParams.AMOUNT);
        ccy                   =  bundle.getString(AppParams.CCY_ID);

        comm_id               = bundle.getString(AppParams.COMM_ID);
        comm_name             = bundle.getString(AppParams.COMM_NAME);
        comm_code             = bundle.getString(AppParams.COMM_CODE);

        desc                  =  bundle.getString(AppParams.DESCRIPTION);
        if (desc != null && !desc.equals("") && !desc.equals("null")){
            desc              =  bundle.getString(AppParams.DESCRIPTION);
        }else{
            desc              =  "";
        }

        TextView lbl_tx_id = (TextView) view.findViewById(R.id.lbl_tx_id);
        lbl_tx_id.setText(tx_id);

        TextView lbl_max_date = (TextView) view.findViewById(R.id.lbl_max_date);
        lbl_max_date.setText(max_tx_date);

        TextView lbl_ccy_id = (TextView) view.findViewById(R.id.lbl_ccy_id);
        lbl_ccy_id.setText(ccy);

        TextView lbl_amount = (TextView) view.findViewById(R.id.lbl_amount);
        if(ccy.equalsIgnoreCase("idr"))
            lbl_amount.setText(FormatCurrency.getRupiahFormat(amount));
        else
            lbl_amount.setText(amount);

        TextView lbl_desc = (TextView) view.findViewById(R.id.lbl_desc);
        lbl_desc.setText(desc);

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                getDialog().dismiss();
            }
        });

        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

}
