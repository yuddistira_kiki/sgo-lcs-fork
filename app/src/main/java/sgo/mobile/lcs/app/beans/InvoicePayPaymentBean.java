package sgo.mobile.lcs.app.beans;

/**
 * Created by thinkpad on 8/3/2015.
 */
public class InvoicePayPaymentBean {
    private String doc_no_pay;
    private String doc_id_pay;
    private String amount_pay;
    private String remain_amount_pay;
    private String input_amount_pay;
    private String ccy_pay;
    private String doc_desc_pay;
    private String session_id_pay;

    public String getAmount_pay() {
        return amount_pay;
    }

    public void setAmount_pay(String amount_pay) {
        this.amount_pay = amount_pay;
    }

    public String getCcy_pay() {
        return ccy_pay;
    }

    public void setCcy_pay(String ccy_pay) {
        this.ccy_pay = ccy_pay;
    }

    public String getDoc_desc_pay() {
        return doc_desc_pay;
    }

    public void setDoc_desc_pay(String doc_desc_pay) {
        this.doc_desc_pay = doc_desc_pay;
    }

    public String getDoc_id_pay() {
        return doc_id_pay;
    }

    public void setDoc_id_pay(String doc_id_pay) {
        this.doc_id_pay = doc_id_pay;
    }

    public String getDoc_no_pay() {
        return doc_no_pay;
    }

    public void setDoc_no_pay(String doc_no_pay) {
        this.doc_no_pay = doc_no_pay;
    }

    public String getInput_amount_pay() {
        return input_amount_pay;
    }

    public void setInput_amount_pay(String input_amount_pay) {
        this.input_amount_pay = input_amount_pay;
    }

    public String getRemain_amount_pay() {
        return remain_amount_pay;
    }

    public void setRemain_amount_pay(String remain_amount_pay) {
        this.remain_amount_pay = remain_amount_pay;
    }

    public String getSession_id_pay() {
        return session_id_pay;
    }

    public void setSession_id_pay(String session_id_pay) {
        this.session_id_pay = session_id_pay;
    }
}
