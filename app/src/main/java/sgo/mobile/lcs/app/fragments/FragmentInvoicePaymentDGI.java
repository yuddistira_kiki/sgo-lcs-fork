package sgo.mobile.lcs.app.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.ui.dialog.DefinedDialog;
import sgo.mobile.lcs.app.ui.edittext.InputFilterMinMax;
import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

public class FragmentInvoicePaymentDGI extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;

    public String member_id, member_code, member_name;
    public String ccy_id, buyer_fee, commission_fee, min_amount, max_amount;
    public String doc_no, doc_id, amount, hold_amount, remain_amount, input_amount, ccy, partial, doc_desc, session_id;
    public String comm_id, comm_name, comm_code, sales_alias,buss_scheme_code;

    public long remain_amt = 0;

    EditText inpAmount;
    Button btnDone;
    Button btnCancel;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_invdgi_payment, container, false);

        inpAmount             = (EditText) view.findViewById(R.id.inpAmount);
        inpAmount.setFilters(new InputFilter[]{ new InputFilterMinMax(AplConstants.MIN_AMOUNT_PAY, AplConstants.MAX_AMOUNT_PAY)});
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnCancel             = (Button) view.findViewById(R.id.btnCancel);

        final Bundle bundle   =  this.getArguments();
        doc_no                =  bundle.getString("doc_no");
        doc_id                =  bundle.getString("doc_id");
        hold_amount           =  bundle.getString("hold_amount");
        amount                =  FormatCurrency.getRupiahFormat(bundle.getString("amount"));
        remain_amount         =  bundle.getString("remain_amount");
        long remain            = Long.parseLong(remain_amount);
        long hold              = Long.parseLong(hold_amount);
        remain_amt            = remain - hold;
        remain_amount         =  FormatCurrency.getRupiahFormat(Long.toString(remain_amt));
        ccy                   =  bundle.getString("ccy");

        String str_partial    = bundle.getString("partial_payment");
        if(str_partial.equalsIgnoreCase("Y")) {
            partial = "Ya";
        }else if(str_partial.equalsIgnoreCase("N")){
            partial = "Tidak";
        }else{
            partial = "Bisa Lebih";
        }

        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        sales_alias           = bundle.getString("sales_alias");
        buss_scheme_code      = bundle.getString("buss_scheme_code");

        doc_desc              =  bundle.getString("doc_desc");
        if (doc_desc != null && !doc_desc.equals("") && !doc_desc.equals("null")){
            doc_desc              =  bundle.getString("doc_desc");
        }else{
            doc_desc              =  "";
        }

        member_id             =  bundle.getString("member_id");
        member_code           =  bundle.getString("member_code");
        member_name           =  bundle.getString("member_name");
        session_id            =  bundle.getString("session_id");

        ccy_id                = bundle.getString("ccy_id");
        buyer_fee             = bundle.getString("buyer_fee");
        commission_fee        = bundle.getString("commission_fee");
        min_amount            = bundle.getString("min_amount");
        max_amount            = bundle.getString("max_amount");

        TextView lbl_doc_no = (TextView) view.findViewById(R.id.lbl_doc_no);
        lbl_doc_no.setText(doc_no);

        TextView lbl_doc_desc = (TextView) view.findViewById(R.id.lbl_doc_desc);
        lbl_doc_desc.setText(doc_desc);

        TextView lbl_amount = (TextView) view.findViewById(R.id.lbl_amount);
        lbl_amount.setText(amount);

        TextView lbl_remain_amount = (TextView) view.findViewById(R.id.lbl_remain_amount);
        lbl_remain_amount.setText(remain_amount);

        TextView lbl_partial = (TextView) view.findViewById(R.id.lbl_partial);
        lbl_partial.setText(partial);

        if(partial.equalsIgnoreCase("Ya") || partial.equalsIgnoreCase("Bisa Lebih")){
            showInvoiceAmount(view);
        }else{
            hideInvoiceAmount(view);
        }

        btnDone.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                input_amount = inpAmount.getText().toString();
                if(partial.equalsIgnoreCase("Ya") || partial.equalsIgnoreCase("Bisa Lebih")){
                    input_amount = inpAmount.getText().toString();
                }else{
                    input_amount = Long.toString(remain_amt);
                }
                if(input_amount.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{
                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Menyimpan Pembayaran...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("doc_id", doc_id);
                    params.put("doc_no", doc_no);
                    params.put("amount", input_amount);
                    params.put("session_id", session_id);
                    Log.d("params", params.toString());
                    client.post(AplConstants.DiSavePayMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                hideKeyboard();

                                Fragment newFragment = null;
                                newFragment = new FragmentInvoiceListDGI();
                                Bundle args = new Bundle();
                                args.putString("member_id", member_id);
                                args.putString("member_code", member_code);
                                args.putString("member_name", member_name);
                                args.putString("session_id_param", session_id);

                                args.putString("comm_id", comm_id);
                                args.putString("comm_name", comm_name);
                                args.putString("comm_code", comm_code);
                                args.putString("sales_alias", sales_alias);
                                args.putString("buss_scheme_code", buss_scheme_code);

                                newFragment.setArguments(args);
                                switchFragment(newFragment);


                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });

                }
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View arg0) {
                hideKeyboard();
                Fragment newFragment = null;
                newFragment = new FragmentInvoiceListDGI();
                Bundle args = new Bundle();
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                args.putString("member_name", member_name);
                args.putString("session_id_param", session_id);

                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("sales_alias", sales_alias);
                args.putString("buss_scheme_code", buss_scheme_code);

                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void showInvoiceAmount(View target){
        int TableId = R.id.tableInvoiceAmount;
        TableRow TableVisibility = (TableRow) target.findViewById(TableId);
        TableVisibility.setVisibility(View.VISIBLE);

        int lineDividerId = R.id.line_divider_amount;
        View lineDivider = (View) target.findViewById(lineDividerId);
        lineDivider.setVisibility(View.VISIBLE);
    }

    public void hideInvoiceAmount(View target){
        int TableId = R.id.tableInvoiceAmount;
        TableRow TableVisibility = (TableRow) target.findViewById(TableId);
        TableVisibility.setVisibility(View.GONE);

        int lineDividerId = R.id.line_divider_amount;
        View lineDivider = (View) target.findViewById(lineDividerId);
        lineDivider.setVisibility(View.GONE);
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

    public void hideKeyboard(){
        View view = getActivity().getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

}