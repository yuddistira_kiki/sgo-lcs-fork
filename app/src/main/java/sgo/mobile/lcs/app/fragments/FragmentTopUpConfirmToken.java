
package sgo.mobile.lcs.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.ui.dialog.DefinedDialog;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

import java.util.Arrays;

public class FragmentTopUpConfirmToken extends Fragment {
    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;
    String transaction_id, transfer_code;


    String account_no, account_name, total;
    String rq_trx_id, rq_desc;

    EditText txtOtp;
    Button btnDone;

    private String  member_id, member_code, tx_id,tx_date, currency, amount, remark, bank_code, bank_name, product_code, product_name, comm_code, comm_name, benef_acct_no, benef_acct_name, tx_amount, buyer_fee, seller_fee, tx_fee, charges_acct_no, fee_acct_no;
    private String payment_id, payment_ref_tx, payment_ref_buyer, payment_ref_seller, payment_status, payment_remark, payment_created, remark_topup;
    private String amount_str, buyer_fee_str, seller_fee_str, tx_fee_str, total_str;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_topup_confirmtoken, container, false);

        txtOtp                = (EditText) view.findViewById(R.id.txtOtp);
        btnDone               = (Button) view.findViewById(R.id.btnDone);

        Bundle bundle         = this.getArguments();

        rq_trx_id             = bundle.getString("Rq_trx_id");
        rq_desc               = bundle.getString("Rq_date");

        tx_id                 = bundle.getString("PaymentId");
        account_no            = bundle.getString("BenefAccNo");
        account_name          = bundle.getString("BenefAccName");

        member_id             = bundle.getString("MemberId");
        member_code             = bundle.getString("MemberCode");
        tx_id                   = bundle.getString("PaymentId");
        bank_code               = bundle.getString("BankCode");
        bank_name               = bundle.getString("BankName");
        product_code            = bundle.getString("ProductCode");
        product_name            = bundle.getString("ProductName");
        comm_code               = bundle.getString("CommCode");
        comm_name               = bundle.getString("CommName");
        benef_acct_no           = bundle.getString("BenefAccNo");
        benef_acct_name         = bundle.getString("BenefAccName");

        amount                  = bundle.getString("Amount");
        amount_str              = FormatCurrency.getRupiahFormat(amount);

        buyer_fee               = bundle.getString("BuyerFee");
        buyer_fee_str           = FormatCurrency.getRupiahFormat(buyer_fee);

        seller_fee              = bundle.getString("SellerFee");
        seller_fee_str          = FormatCurrency.getRupiahFormat(seller_fee);

        tx_fee                  = bundle.getString("TxFee");
        tx_fee_str              = FormatCurrency.getRupiahFormat(tx_fee);

        charges_acct_no         = bundle.getString("ChargesAcctNo");
        fee_acct_no             = bundle.getString("FeeAcctNo");

        total                   = bundle.getString("Total");
        total_str               = FormatCurrency.getRupiahFormat(total);

        remark                  = bundle.getString("Remark");


        TextView lbl_community =(TextView) view.findViewById(R.id.lbl_community);
        lbl_community.setText(comm_name);

        TextView lbl_member =(TextView) view.findViewById(R.id.lbl_member);
        lbl_member.setText(member_code);

        TextView lbl_bank =(TextView) view.findViewById(R.id.lbl_bank);
        lbl_bank.setText(bank_name);

        TextView lbl_bank_product =(TextView) view.findViewById(R.id.lbl_bank_product);
        lbl_bank_product.setText(product_name);


        TextView lbl_account_no =(TextView) view.findViewById(R.id.lbl_account_no);
        lbl_account_no.setText(account_no);

        TextView lbl_account_name =(TextView) view.findViewById(R.id.lbl_account_name);
        lbl_account_name.setText(account_name);

        TextView lbl_jumlah         = (TextView) view.findViewById(R.id.lbl_jumlah);
        lbl_jumlah.setText(amount_str);

        TextView lbl_buyer_fee    = (TextView) view.findViewById(R.id.lbl_buyer_fee);
        lbl_buyer_fee.setText(buyer_fee_str);

        TextView lbl_seller_fee    = (TextView) view.findViewById(R.id.lbl_seller_fee);
        lbl_seller_fee.setText(seller_fee_str);

        TextView lbl_message    = (TextView) view.findViewById(R.id.lbl_message);
        lbl_message.setText(remark);

        TextView lbl_jumlah_total    = (TextView) view.findViewById(R.id.lbl_jumlah_total);
        lbl_jumlah_total.setText(total_str);



        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // TODO Auto-generated method stub


                transfer_code        = txtOtp.getText().toString();
                if(transfer_code.equalsIgnoreCase(""))
                {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                }else{

                    String mStringArray[] = { rq_trx_id };
                    JSONArray mJSONArray  = new JSONArray(Arrays.asList(mStringArray));
                    String data_inv       = mJSONArray.toString();

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing Token Payment To Principal...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("token_id", transfer_code);
                    params.put("flag_reversal", "N");
                    params.put("data_inv", data_inv);

                    Log.d("params", params.toString());
                    client.post(AplConstants.ConfirmTokenEw9, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    String source_acct_no     = object.getString("source_acct_no");
                                    String source_acct_name   = object.getString("source_acct_name");

                                    JSONArray arrDataReqToken = object.getJSONArray("data");
                                    String trx_reason         = arrDataReqToken.getJSONObject(0).getString("trx_reason");
                                    String trx_id             = arrDataReqToken.getJSONObject(0).getString("trx_id");
                                    String trx_date           = arrDataReqToken.getJSONObject(0).getString("trx_date");
                                    String ccy_id             = arrDataReqToken.getJSONObject(0).getString("ccy_id");
                                    String trx_status         = arrDataReqToken.getJSONObject(0).getString("trx_status");
                                    String trx_amount         = arrDataReqToken.getJSONObject(0).getString("trx_amount");
                                    String external_trx       = arrDataReqToken.getJSONObject(0).getString("external_trx");
                                    String external_buyer     = arrDataReqToken.getJSONObject(0).getString("external_buyer");
                                    String external_seller    = arrDataReqToken.getJSONObject(0).getString("external_seller");

                                    payment_id = trx_id;
                                    amount = trx_amount;
                                    payment_ref_tx = external_trx;
                                    payment_ref_buyer = external_buyer;
                                    payment_ref_seller = external_seller;
                                    payment_status = trx_status;
                                    payment_remark = trx_reason;
                                    payment_created = trx_date;

                                    if(trx_status.equalsIgnoreCase("S")){
                                        pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Processing Payment Report...");
                                        AsyncHttpClient client = new AsyncHttpClient();
                                        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                                        RequestParams params   = new RequestParams();

                                        params.put("member_id", member_id);
                                        params.put("source_acct_no", source_acct_no);
                                        params.put("source_acct_name", source_acct_name);
                                        params.put("bank_code", bank_code);
                                        params.put("product_code", product_code);
                                        params.put("ccy_id", ccy_id);
                                        params.put("seller_fee", seller_fee);
                                        params.put("buyer_fee", buyer_fee);
                                        params.put("tx_fee", tx_fee);
                                        params.put("benef_acct_no", benef_acct_no);
                                        params.put("benef_acct_name", benef_acct_name);
                                        params.put("charges_acct_no", charges_acct_no);
                                        params.put("fee_acct_no", fee_acct_no);
                                        params.put("payment_id", payment_id);
                                        params.put("amount", amount);
                                        params.put("payment_ref_tx", payment_ref_tx);
                                        params.put("payment_ref_buyer", payment_ref_buyer);
                                        params.put("payment_ref_seller", payment_ref_seller);
                                        params.put("payment_status", payment_status);
                                        params.put("payment_remark", payment_remark);
                                        params.put("payment_created", payment_created);
                                        params.put("remark_topup", remark);

                                        Log.d("params", params.toString());

                                        client.post(AplConstants.TopUpPaymentMobileAPI, params, new AsyncHttpResponseHandler() {
                                            public void onSuccess(String content) {
                                                Log.d("result:", content);
                                                try {
                                                    JSONObject object         = new JSONObject(content);

                                                    String error_code         = object.getString("error_code");
                                                    String error_msg          = object.getString("error_message");

                                                    if (pDialog != null) {
                                                        pDialog.dismiss();
                                                    }
                                                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                                        String tx_id          = object.getString("tx_id");

                                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                        alert.setTitle("Payment To Principal To Principal");
                                                        alert.setMessage("Payment To Principal To Principal : Success");
                                                        alert.setPositiveButton("OK", null);
                                                        alert.show();

                                                        Fragment newFragment = null;
                                                        newFragment = new FragmentTopUpFinish();
                                                        Bundle args = new Bundle();
                                                        args.putString("PaymentId", tx_id);
                                                        args.putString("MemberId", member_id);
                                                        args.putString("MemberCode", member_code);
                                                        args.putString("BankCode", bank_code);
                                                        args.putString("BankName", bank_name);
                                                        args.putString("ProductCode", product_code);
                                                        args.putString("ProductName", product_name);
                                                        args.putString("CommCode", comm_code);
                                                        args.putString("CommName", comm_name);
                                                        args.putString("BenefAccNo", benef_acct_no);
                                                        args.putString("BenefAccName", benef_acct_name);
                                                        args.putString("Amount", amount_str);
                                                        args.putString("BuyerFee", buyer_fee_str);
                                                        args.putString("SellerFee", seller_fee_str);
                                                        args.putString("TxFee", tx_fee_str);
                                                        args.putString("ChargesAcctNo", charges_acct_no);
                                                        args.putString("FeeAcctNo", fee_acct_no);
                                                        args.putString("Remark", remark);
                                                        args.putString("Total", total_str);
                                                        newFragment.setArguments(args);
                                                        switchFragment(newFragment);

                                                    }else{
                                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                        alert.setTitle("Payment To Principal To Principal");
                                                        alert.setMessage("Payment To Principal To Principal : " + error_msg);
                                                        alert.setPositiveButton("OK", null);
                                                        alert.show();
                                                    }
                                                } catch (JSONException e) {
                                                    // TODO Auto-generated catch block
                                                    e.printStackTrace();
                                                }
                                            };

                                            public void onFailure(Throwable error, String content) {
                                                if (pDialog != null) {
                                                    pDialog.dismiss();
                                                }
                                                Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                            }                                        });

                                    }else{

                                        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                        alert.setTitle("Payment To Principal To Principal");
                                        alert.setMessage("Payment To Principal To Principal : " + trx_reason);
                                        alert.setPositiveButton("OK", null);
                                        alert.show();
                                    }

                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Payment To Principal To Principal");
                                    alert.setMessage("Payment To Principal To Principal : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        return view;
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
