package sgo.mobile.lcs.app.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockActivity;
import org.json.JSONException;
import org.json.JSONObject;

import sgo.mobile.lcs.BuildConfig;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;


public class SplashScreen extends SherlockActivity {
//    ProgressBar prgLoading;
    int progress = 0;
//    double versionCode = 0.0;
    boolean isUpdate = false;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        InitializeToolbar();
        setContentView(R.layout.splash_screen);

//        try {
//            versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
//            Log.d("version code", Double.toString(versionCode));
//        } catch (PackageManager.NameNotFoundException e) {
//            e.printStackTrace();
//        }

        getAppVersion();

//        prgLoading = (ProgressBar) findViewById(R.id.prgLoading);
//        prgLoading.setProgress(progress);

        new Loading().execute();
    }

    public void InitializeToolbar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            actionBar.setHomeButtonEnabled(false);
        }

    }

    /** this class is used to handle thread */
    public class Loading extends AsyncTask<Void, Void, Void>{

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // TODO Auto-generated method stub
            while(progress < 100){
                try {
                    Thread.sleep(1000);
                    progress += 30;
//                    prgLoading.setProgress(progress);
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // TODO Auto-generated method stub
            //Intent i = new Intent(SplashScreen.this, ActivityLogin.class);
            if(!isUpdate) {
                Intent i = new Intent(SplashScreen.this, ActivityLogin.class);
                startActivity(i);
            }
        }
    }

    public void getAppVersion(){
        try{
            AsyncHttpClient client = new AsyncHttpClient();
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
            client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

            client.get(AplConstants.ServiceApp, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                    Log.d("result service app:", content);
                    try {
                        JSONObject response = new JSONObject(content);
                        String code = response.getString(AppParams.ERROR_CODE);
                        if (code.equals(AppParams.SUCCESS_CODE)) {
                            String arrayApp = response.getString(AppParams.APP_DATA);
                            final JSONObject mObject = new JSONObject(arrayApp);
                            String package_version = mObject.getString(AppParams.PACKAGE_VERSION);
                            final String package_name = mObject.getString(AppParams.PACKAGE_NAME);
                            final String type = mObject.getString(AppParams.TYPE);
                            String app_id = mObject.getString(AppParams.APP_ID);

                            if (!package_version.equalsIgnoreCase(BuildConfig.VERSION_NAME)) {
                                isUpdate = true;
                                AlertDialog.Builder builder = new AlertDialog.Builder(SplashScreen.this)
                                        .setTitle("Update")
                                        .setMessage("Application is out of date,  Please update immediately")
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .setCancelable(false)
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                final String appPackageName = package_name;
                                                if (type.equalsIgnoreCase("1")) {
                                                    try {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                                    } catch (android.content.ActivityNotFoundException anfe) {
                                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                                    }
                                                } else if (type.equalsIgnoreCase("2")) {
                                                    String download_url = null;
                                                    try {
                                                        download_url = mObject.getString(AppParams.DOWNLOAD_URL);
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(download_url)));
                                                }
                                                SplashScreen.this.finish();
                                                android.os.Process.killProcess(android.os.Process.myPid());
                                                System.exit(0);
                                                getParent().finish();
                                            }
                                        });
                                AlertDialog alertDialog = builder.create();
                                if (!isFinishing())
                                    alertDialog.show();
                            }
                        } else {
                            code = response.getString(AppParams.ERROR_MESSAGE);
                            Toast.makeText(getApplicationContext(), code, Toast.LENGTH_LONG).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                ;

                public void onFailure(Throwable error, String content) {
                    Log.w("Error Koneksi get App Version", error.toString());
                    Toast.makeText(SplashScreen.this, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }

                ;
            });

        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }
}