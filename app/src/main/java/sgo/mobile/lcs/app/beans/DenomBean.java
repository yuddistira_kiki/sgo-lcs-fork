package sgo.mobile.lcs.app.beans;

public class DenomBean {

    public String id    = "";
    public String price = "";
    public String name  = "";

    public DenomBean( String _id, String _price, String _name )
    {
        id     = _id;
        price  = _price;
        name   = _name;
    }

    public String getId(){
        return id;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getPrice(){
        return price;
    }

    public void setPrice(String price){
        this.price = price;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String toString()
    {
        return( name + " (Rp." + price + ")" );
    }
}