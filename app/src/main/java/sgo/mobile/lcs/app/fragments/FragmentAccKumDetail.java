package sgo.mobile.lcs.app.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.ui.dialog.DefinedDialog;
import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

/**
 * Created by thinkpad on 3/29/2016.
 */
public class FragmentAccKumDetail extends Fragment {

    private ProgressDialog pDialog;
    Activity act;

    TextView txtBankName, txtAccNo, txtAccName, txtCurrency, txtLimit;
    Button btnBack;
    String accNo, accName, bankCode, bankName, cust_id;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account_kum_detail, container, false);

        act = getActivity();

        txtBankName = (TextView) view.findViewById(R.id.acckumdetail_bank_name);
        txtAccNo = (TextView) view.findViewById(R.id.acckumdetail_acc_no);
        txtAccName = (TextView) view.findViewById(R.id.acckumdetail_acc_name);
        txtCurrency = (TextView) view.findViewById(R.id.acckumdetail_currency);
        txtLimit = (TextView) view.findViewById(R.id.acckumdetail_limit);
        btnBack = (Button) view.findViewById(R.id.btn_back);

        Bundle bundle = this.getArguments();
        accNo = bundle.getString(AppParams.ACCOUNT_NO);
        accName = bundle.getString(AppParams.ACCOUNT_NAME);
        bankCode = bundle.getString(AppParams.BANK_CODE);
        bankName = bundle.getString(AppParams.BANK_NAME);
        cust_id = bundle.getString(AppParams.MEMBER_CUST_ID, "");

        getLimitKum();

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment newFragment = new FragmentListAccKum();
                Bundle args = new Bundle();
                args.putString(AppParams.MEMBER_CUST_ID, cust_id);
                newFragment.setArguments(args);
                switchFragment(newFragment);
            }
        });

        return view;
    }

    public void getLimitKum() {
        pDialog = DefinedDialog.CreateProgressDialog(act, pDialog, "Meminta limit...");

        AsyncHttpClient client = new AsyncHttpClient();
        client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
        client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

        RequestParams params = new RequestParams();
        params.put("cust_id", cust_id);
        params.put("acct_no", accNo);
        params.put("bank_code", bankCode);

        Log.d("params", params.toString());
        client.post(AplConstants.LimitMandiriKUM, params, new AsyncHttpResponseHandler() {
            public void onSuccess(String content) {
                Log.d("result:", content);
                try {
                    // parse json data and store into arraylist variables
                    JSONObject json = new JSONObject(content);
                    String error_code = json.getString("error_code");
                    String error_message = json.getString("error_message");

                    if (pDialog != null) {
                        pDialog.dismiss();
                    }

                    if (error_code.equals(AppParams.SUCCESS_CODE)) {
                        String limit = json.getString("acct_limit");
                        String ccy_id = json.getString("ccy_id");

                        txtBankName.setText(bankName);
                        txtAccNo.setText(accNo);
                        txtAccName.setText(accName);
                        txtCurrency.setText(ccy_id);
                        if(ccy_id.equalsIgnoreCase("idr")) {
                            txtLimit.setText(FormatCurrency.getRupiahFormat(limit));
                        }
                        else txtLimit.setText(limit);

                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(act);
                        builder.setMessage(error_message)
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Fragment newFragment = new FragmentListAccKum();
                                        Bundle args = new Bundle();
                                        args.putString(AppParams.MEMBER_CUST_ID, cust_id);
                                        newFragment.setArguments(args);
                                        switchFragment(newFragment);
                                    }
                                });
                        AlertDialog alert = builder.create();
                        alert.show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();

                }
            }

            public void onFailure(Throwable error, String content) {
                Toast.makeText(act, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }
}
