package sgo.mobile.lcs.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.fragments.FragmentInvoiceListDGI;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;

import java.util.StringTokenizer;

public class InvoiceDGIAdapter extends BaseAdapter {
    private Activity activity;

    public InvoiceDGIAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentInvoiceListDGI.doc_no.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_invdgi_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        long remain = Long.parseLong(FragmentInvoiceListDGI.remain_amount.get(position));
        long hold   = Long.parseLong(FragmentInvoiceListDGI.hold_amount.get(position));
        long remain_amount = remain - hold;
        String remain_amount_string  = Long.toString(remain_amount);
        StringTokenizer tokens = new StringTokenizer(remain_amount_string, ".");
        String first = tokens.nextToken();
//        Log.d("remain_string", Long.toString(remain));
//        Log.d("hold_string", Long.toString(hold));
//        Log.d("remain_amount_string", Long.toString(remain_amount));
//        Log.d("first", first);

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText("Invoice " + FragmentInvoiceListDGI.doc_no.get(position));
        holder.txtSubText.setText( "Sisa : " +  FormatCurrency.getRupiahFormat(first));

        String InputAmount = FragmentInvoiceListDGI.input_amount.get(position);
        if (InputAmount != null && !InputAmount.equals("") && !InputAmount.equals("null") && Long.parseLong(InputAmount) > 0){
            holder.txtSubText2.setVisibility(View.VISIBLE);
            holder.txtSubText2.setText( "Bayar : " +  FormatCurrency.getRupiahFormat(InputAmount));
        }else{
            holder.txtSubText2.setVisibility(View.GONE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtSubText2;
    }
}
