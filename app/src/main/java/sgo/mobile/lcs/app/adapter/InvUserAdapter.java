package sgo.mobile.lcs.app.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.fragments.FragmentInvUserList;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;

import java.util.StringTokenizer;

public class InvUserAdapter extends BaseAdapter {
    private Activity activity;

    public InvUserAdapter(Activity act) {
        this.activity = act;
    }
    public int getCount() {
        // TODO Auto-generated method stub
        return FragmentInvUserList.doc_no.size();
    }

    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ViewHolder holder;

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.fragment_invdgi_list_item, null);
            holder = new ViewHolder();

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }


        long remain = Long.parseLong(FragmentInvUserList.remain_amount.get(position));
        long hold   = Long.parseLong(FragmentInvUserList.hold_amount.get(position));
        long remain_amount = remain - hold;
        String remain_amount_string  = Long.toString(remain_amount);
        StringTokenizer tokens = new StringTokenizer(remain_amount_string, ".");
        String first = tokens.nextToken();

        holder.txtText    = (TextView) convertView.findViewById(R.id.txtText);
        holder.txtSubText = (TextView) convertView.findViewById(R.id.txtSubText);
        holder.txtSubText2 = (TextView) convertView.findViewById(R.id.txtSubText2);

        holder.txtText.setText("Invoice " + FragmentInvUserList.doc_no.get(position));
        holder.txtSubText.setText( "Sisa : " +  FormatCurrency.getRupiahFormat(first));

        String InputAmount = FragmentInvUserList.input_amount.get(position);
        if (InputAmount != null && !InputAmount.equals("") && !InputAmount.equals("null") && Integer.parseInt(InputAmount) > 0){
//            String buyer_fee      = FragmentInvUserList.buyer_fee;
//            String buyer_fee      = "0";
//            buyer_fee             = (buyer_fee != null && !buyer_fee.equals("") && !buyer_fee.equals("null") && Integer.parseInt(buyer_fee) > 0)? buyer_fee : "0";
//            int total_pay_plus_fee     = Integer.parseInt(InputAmount) + Integer.parseInt(buyer_fee);
            int total_pay_plus_fee     = Integer.parseInt(InputAmount);
            holder.txtSubText2.setVisibility(View.VISIBLE);
//            holder.txtSubText2.setText( "Bayar : " +  FormatCurrency.getRupiahFormat(Integer.toString(total_pay_plus_fee)) + " (Fee : " + FormatCurrency.getRupiahFormat(buyer_fee) + ")");
            holder.txtSubText2.setText( "Bayar : " +  FormatCurrency.getRupiahFormat(Integer.toString(total_pay_plus_fee)));
        }else{
            holder.txtSubText2.setVisibility(View.GONE);
        }

        return convertView;
    }

    static class ViewHolder {
        TextView txtText, txtSubText, txtSubText2;
    }
}
