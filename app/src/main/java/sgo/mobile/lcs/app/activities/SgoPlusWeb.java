package sgo.mobile.lcs.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.*;
import android.webkit.*;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.view.Menu;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.dialogs.DefinedDialog;
import sgo.mobile.lcs.app.dialogs.ReportBillerDialog;
import sgo.mobile.lcs.app.fragments.FragmentPaymentRepayment;
import sgo.mobile.lcs.app.fragments.FragmentPreviewTransaksi;
import sgo.mobile.lcs.app.fragments.FragmentReportEspay;
import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Random;

/*
  Created by Administrator on 11/5/2014.
 */
public class SgoPlusWeb extends BaseMainActivity implements ReportBillerDialog.OnDialogOkCallback {

    private WebView webview;
    private String app_name = "SGO Plus Payment Gateaway";
    private String SGO_PLUS_URL = "";
    String masterDomainSGOplus;
    String devDomainSGOPlus = "https://sandbox-kit.espay.id/";
    String prodDomainSGOPlus = "https://kit.espay.id/";
    String prodDomainSGOPlusMandiri = "https://scm.bankmandiri.co.id/sgo+/";
    String bankName, bankProduct,bankCode;
    Boolean isBCA, isDisconnected;
    Intent mIntent;
    ProgressDialog out;
    String tx_multiple, repayment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sgo_plus_web);
        InitializeToolbar();

        isDisconnected = !isOnline();
        out = DefinedDialog.CreateProgressDialog(SgoPlusWeb.this, null);

        mIntent   = getIntent();
        String productCode  = mIntent.getStringExtra(AppParams.PRODUCT_CODE);
        bankProduct = mIntent.getStringExtra(AppParams.PRODUCT_NAME);
        bankCode = mIntent.getStringExtra(AppParams.BANK_CODE);
        bankName = mIntent.getStringExtra(AppParams.BANK_NAME);
        String commCode = mIntent.getStringExtra(AppParams.COMM_CODE);
        String paymentId = mIntent.getStringExtra(AppParams.TX_ID);
        String apikey  = mIntent.getStringExtra(AppParams.API_KEY);
        String comm_id = mIntent.getStringExtra(AppParams.COMM_ID);
        String amount = mIntent.getStringExtra(AppParams.TX_AMOUNT);
        String fee = mIntent.getStringExtra(AppParams.ADMIN_FEE);
        String total = mIntent.getStringExtra(AppParams.TOTAL_PAY);
        tx_multiple = mIntent.getStringExtra(AppParams.TX_MULTIPLE);
        repayment = mIntent.getStringExtra(AppParams.REPAYMENT);

        Log.d("isi intent", mIntent.getExtras().toString());

        if(AplConstants.IS_PROD){
            if(bankCode.equals("008"))masterDomainSGOplus = prodDomainSGOPlusMandiri;
            else masterDomainSGOplus = prodDomainSGOPlus;
        }
        else masterDomainSGOplus = devDomainSGOPlus;


        if(tx_multiple.equalsIgnoreCase("Y")) {
            SGO_PLUS_URL = masterDomainSGOplus + "multi/order" +
                    "?paymentId=" + paymentId +
                    "&commCode=GOWORLD" +
                    "&bankCode=" + bankCode +
                    "&productCode=" + productCode + "&mobile=1";
        }
        else if(tx_multiple.equalsIgnoreCase("N")) {
            SGO_PLUS_URL = masterDomainSGOplus + "index/order/?key=" + apikey +
                    "&paymentId=" + paymentId +
                    "&commCode=" + commCode +
                    "&bankCode=" + bankCode +
                    "&productCode=" + productCode + "&mobile=1";
        }

        Log.d("url sgo plus", SGO_PLUS_URL);

        if(!bankCode.equals("008")){
            try {
                String callbackUrl = AplConstants.CallbackURL;
                if(!callbackUrl.isEmpty())
                    SGO_PLUS_URL = SGO_PLUS_URL + "&url=" + URLEncoder.encode(callbackUrl+"?refid=" + gen_numb() + "&ref_back_url=" + productCode + "&isclose=1", "utf-8");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        loadUrl(SGO_PLUS_URL, paymentId, comm_id, amount, fee, total);
//        setResult(MainActivity.RESULT_NORMAL);

    }

    public void InitializeToolbar()
    {
//        ActionBar actionBar = getSupportActionBar();
//        actionBar.setDisplayShowTitleEnabled(true);
//        actionBar.setTitle(R.string.ab_title);
//        actionBar.setSubtitle(R.string.internet_banking);
//        actionBar.setHomeButtonEnabled(false);
//        actionBar.setDisplayHomeAsUpEnabled(false);


        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowTitleEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            actionBar.setHomeButtonEnabled(false);
        }

        LayoutInflater inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflator.inflate(R.layout.toolbar_main, null);

        actionBar.setCustomView(v);
        actionBar.setDisplayShowCustomEnabled(true);
    }

    public boolean isOnline() {
        ConnectivityManager connMgr = (ConnectivityManager)this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public int gen_numb() {
        Random r = new Random( System.currentTimeMillis() );
        return ((1 + r.nextInt(9)) * 100000000 + r.nextInt(100000000));
    }

    @SuppressWarnings("deprecation")
    public void loadUrl(String url, final String payment_id, final String commId, final String amount, final String fee, final String total) {
        webview = (WebView) findViewById(R.id.webview);
        WebSettings webSettings = webview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setCacheMode(WebSettings.LOAD_NO_CACHE);
        if (android.os.Build.VERSION.SDK_INT<=11) {
            webSettings.setAppCacheMaxSize(1024 * 1024 * 8);
        }

        final Activity activity = this;
        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                //setSupportProgress(progress * 100);
                out.show();
                //activity.setProgress(progress * 100);
                if(progress == 100) out.dismiss();
            }
        });
        webview.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.d("isi url tombolnya", url);
                if (url.contains("isclose=1")){
                    getTrxStatus( payment_id, commId, amount, fee, total);
                }
                else if(url.contains("isback=1")) {
                    onOkButton();
                }
                else view.loadUrl(url);

                return true;
            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
                isDisconnected = true;
                invalidateOptionsMenu();

            }

            @Override
            public void onReceivedSslError(WebView view, @NonNull SslErrorHandler handler, SslError error) {
                if(AplConstants.IS_PROD)
                    super.onReceivedSslError(view, handler, error);
                else
                    handler.proceed();
            }

            @Override
            public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
                super.onReceivedClientCertRequest(view, request);
            }
        });


        webview.loadUrl(url);
    }


    public void getTrxStatus(final String txId, final String comm_id, final String amount, final String fee, final String total){
        try{
            out.show();
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(AplConstants.HTTP_LONG_TIMEOUT);
            client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());

            RequestParams params = new RequestParams();
            params.put(AppParams.TX_ID, txId);
            params.put(AppParams.COMM_ID, comm_id);
            params.put(AppParams.TX_MULTIPLE, tx_multiple);

            Log.d("params get Trx Status", params.toString());

            client.post(AplConstants.TrxStatusAPI, params, new AsyncHttpResponseHandler() {
                public void onSuccess(String content) {
                try {
                    out.dismiss();
                    Log.d("response get Trx Status", content);

                    JSONObject response = new JSONObject(content);
                    String code = response.getString(AppParams.ERROR_CODE);
                    if (code.equals(AppParams.SUCCESS_CODE)) {
                        if(tx_multiple.equalsIgnoreCase("N")) {
                            String date = response.getString(AppParams.CREATED);
                            String tx_id = response.getString(AppParams.TX_ID);
                            String member_cust_id = response.getString(AppParams.MEMBER_CUST_ID);
                            String member_cust_name = response.getString(AppParams.MEMBER_CUST_NAME);
                            String bank_name = response.getString(AppParams.BANK_NAME);
                            String product_name = response.getString(AppParams.PRODUCT_NAME);
                            String tx_status = response.getString(AppParams.TX_STATUS);
                            String tx_remark = response.getString(AppParams.TX_REMARK);

                            showReportDialog(date, tx_id, member_cust_id, member_cust_name, bank_name, product_name,
                                    amount, fee, total, tx_status, tx_remark);
                        }
                        else if(tx_multiple.equalsIgnoreCase("Y")) {
                            String data_detail = response.getString(AppParams.DATA_DETAIL);
                            if(!data_detail.equals("")) {
                                showReportActivity(data_detail, fee);
                            }
                            else {
                                String msg = "Detail Data is Empty";
                                showDialog(msg);
                            }
                        }

                    } else {
                        String msg = response.getString(AppParams.ERROR_MESSAGE);
                        showDialog(msg);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            public void onFailure(Throwable error, String content) {
                Toast.makeText(SgoPlusWeb.this, "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
            }
        });
        }catch (Exception e){
            Log.d("httpclient", e.getMessage());
        }
    }

    void showDialog(String msg) {
        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_notification);

        // set values for custom dialog components - text, image and button
        Button btnDialogOTP = (Button)dialog.findViewById(R.id.btn_dialog_notification_ok);
        TextView Title = (TextView)dialog.findViewById(R.id.title_dialog);
        TextView Message = (TextView)dialog.findViewById(R.id.message_dialog);

        Message.setVisibility(View.VISIBLE);
        Title.setText(getString(R.string.error));
        Message.setText(msg);

        btnDialogOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if(repayment.equalsIgnoreCase("Y")) {
                    FragmentPaymentRepayment.changeFragment = true;
                }
                else {
                    FragmentPreviewTransaksi.changeFragment = true;
                }
                SgoPlusWeb.this.finish();
            }
        });

        dialog.show();
    }

    private void showReportActivity(String data_detail, String fee){
        FragmentReportEspay fragment = (FragmentReportEspay)getSupportFragmentManager().findFragmentByTag("report_fragment");
        if(fragment == null) {
            fragment = FragmentReportEspay.newInstance(data_detail, fee,"IB");
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(android.R.id.content, fragment, "report_fragment");
            ft.commit();
        }
    }


    private void showReportDialog(String date, String txId, String custID, String custName, String bankName,String productName,
                                        String amount, String fee, String total, String txStatus, String txRemark) {
        Bundle args = new Bundle();
        ReportBillerDialog dialog = ReportBillerDialog.newInstance(this, true);
        args.putString(AppParams.DATE_TIME,date);
        args.putString(AppParams.TX_ID, txId);
        args.putString(AppParams.MEMBER_CUST_ID, custID);
        args.putString(AppParams.MEMBER_CUST_NAME, custName);
        args.putString(AppParams.BANK_NAME, bankName);
        args.putString(AppParams.PRODUCT_NAME, productName);
        args.putString(AppParams.AMOUNT, amount);
        args.putString(AppParams.ADMIN_FEE, fee);
        args.putString(AppParams.TOTAL_PAY, total);
        args.putString(AppParams.TX_STATUS, txStatus);
        args.putString(AppParams.TX_REMARK, txRemark);
        dialog.setArguments(args);
        dialog.show(getSupportFragmentManager(), ReportBillerDialog.TAG);
    }

    @Override
    public void onDetachedFromWindow() {
        /*setVisible(false);
        if(webview != null){
            ViewGroup view = (ViewGroup) getWindow().getDecorView();
            view.removeAllViews();
            webview.destroy();
            webview = null;
        }
        */
        super.onDetachedFromWindow();
    }

    @Override
    public void finish() {
        if(repayment.equalsIgnoreCase("Y")) {
            FragmentPaymentRepayment.changeFragment = true;
        }
        else {
            FragmentPreviewTransaksi.changeFragment = true;
        }
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        view.removeAllViews();
        super.finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(event.getAction() == KeyEvent.ACTION_DOWN){
            switch(keyCode)
            {
                case KeyEvent.KEYCODE_BACK:
                    //if(webview.canGoBack()){
                    //    webview.goBack();
                    //}else{
                    //    finish();
                    //}
                    finish();
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        return super.onPrepareOptionsMenu(menu);
    }
    @Override
    protected void onDestroy() {
        /*
        setVisible(false);
        if(webview != null){
            ViewGroup view = (ViewGroup) getWindow().getDecorView();
            view.removeAllViews();
            webview.destroy();
            webview = null;
        }*/
        super.onDestroy();
    }

    @Override
    public void onOkButton() {
        finish();
    }

}