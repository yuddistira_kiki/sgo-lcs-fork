package sgo.mobile.lcs.app.beans;

public class IndustryBean {
    public String code   = "";
    public String name = "";

    public IndustryBean( String _code, String _name )
    {
        code = _code;
        name = _name;
    }

    public String getCode(){
        return code;
    }

    public void setCode(String code){
        this.code = code;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String toString()
    {
        return( name  );
    }
}