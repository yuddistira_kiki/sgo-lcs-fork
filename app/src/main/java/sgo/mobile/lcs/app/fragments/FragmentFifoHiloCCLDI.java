package sgo.mobile.lcs.app.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import sgo.mobile.lcs.conf.AplConstants;
import sgo.mobile.lcs.conf.AppParams;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.MySSLSocketFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import sgo.mobile.lcs.R;
import sgo.mobile.lcs.app.activities.MainActivity;
import sgo.mobile.lcs.app.adapter.BankCCLAdapter;
import sgo.mobile.lcs.app.beans.BankCCLBean;
import sgo.mobile.lcs.app.ui.dialog.DefinedDialog;
import sgo.mobile.lcs.frameworks.math.FormatCurrency;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpClient;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.AsyncHttpResponseHandler;
import sgo.mobile.lcs.frameworks.net.loopj.android.http.RequestParams;

import java.util.ArrayList;
import java.util.Arrays;

public class FragmentFifoHiloCCLDI extends Fragment {

    private ProgressDialog pDialog;
    FragmentManager fm;
    String Result;

    public String amount;

    public String comm_id, comm_name, comm_code, buss_scheme_code;
    public String member_id, member_code;
    public String sales_id, sales_name, sales_alias, flag_invoice;
    public String bank_data;
    public String hold_amount, ccy_id;

    Button btnDone;
    Button btnCancel;
    EditText inpAmount;

    public String data_inv, bank_code, bank_name, product_code, no_phone_source, benef_acct_no, benef_acct_name, tx_fee, fee_acct_no, charges_acct_no, buyer_fee, seller_fee, commission_fee, commission_acct_no, bbs_product;
    private String[] bank_code_arr, bank_name_arr, ccy_id_arr, product_code_arr, member_phone_arr, benef_acct_no_arr, benef_acct_name_arr, tx_fee_arr, fee_acct_no_arr, seller_fee_arr, charges_acct_no_arr, buyer_fee_arr, commission_acct_no_arr, commission_fee_arr, bbs_product_json_arr;
    private static Spinner cbo_bank      = null;
    private ArrayList<BankCCLBean> BankList = new ArrayList<BankCCLBean>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fifo_hilo_di_ccl, container, false);
        cbo_bank              = (Spinner)view.findViewById(R.id.cbo_bank);

        final Bundle bundle         = this.getArguments();
        comm_id               = bundle.getString("comm_id");
        comm_name             = bundle.getString("comm_name");
        comm_code             = bundle.getString("comm_code");
        buss_scheme_code      = bundle.getString("buss_scheme_code");
        member_code           = bundle.getString("member_code");
        member_id             = bundle.getString("member_id");

        sales_id              = bundle.getString("sales_id");
        sales_name            = bundle.getString("sales_name");
        sales_alias           = bundle.getString("sales_alias");
        flag_invoice          = bundle.getString("flag_invoice");

        hold_amount           = bundle.getString("hold_amount");
        ccy_id                = bundle.getString("ccy_id");
        bank_data             = bundle.getString("bank_data");

        TextView lbl_sales_name = (TextView) view.findViewById(R.id.lbl_sales_name);
        lbl_sales_name.setText(sales_name);

        Double dbl_hold_amount = Double.parseDouble(hold_amount);
        int int_hold_amount    = dbl_hold_amount.intValue();

        TextView lbl_sales_amount = (TextView) view.findViewById(R.id.lbl_sales_amount);
        lbl_sales_amount.setText(FormatCurrency.getRupiahFormat(Integer.toString(int_hold_amount)));

        inpAmount             = (EditText) view.findViewById(R.id.inpAmount);
        btnDone               = (Button) view.findViewById(R.id.btnDone);
        btnDone.setOnClickListener(new View.OnClickListener() {

            public void onClick(final View arg0) {
                amount = inpAmount.getText().toString();
                if (amount.equalsIgnoreCase("")) {
                    Toast.makeText(getActivity(), R.string.form_alert, Toast.LENGTH_SHORT).show();
                } else {
                    BankCCLBean bankSelected  = (BankCCLBean) cbo_bank.getSelectedItem();
                    bank_code             = String.valueOf(bankSelected.bank_code);
                    bank_name             = String.valueOf(bankSelected.bank_name);
                    product_code          = String.valueOf(bankSelected.product_code);
                    ccy_id                = String.valueOf(bankSelected.ccy_id);
                    no_phone_source       = String.valueOf(bankSelected.member_phone);
                    benef_acct_no         = String.valueOf(bankSelected.benef_acct_no);
                    benef_acct_name       = String.valueOf(bankSelected.benef_acct_name);
                    fee_acct_no           = String.valueOf(bankSelected.fee_acct_no);
                    charges_acct_no       = String.valueOf(bankSelected.charges_acct_no);
                    buyer_fee             = String.valueOf(bankSelected.buyer_fee);
                    seller_fee            = String.valueOf(bankSelected.seller_fee);
                    commission_fee        = String.valueOf(bankSelected.commission_fee);
                    commission_acct_no    = String.valueOf(bankSelected.commission_acct_no);
                    tx_fee                = String.valueOf(bankSelected.tx_fee);
                    bbs_product           = String.valueOf(bankSelected.bbs_product_json);

                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Validasi Data Payment Point...");
                    AsyncHttpClient client = new AsyncHttpClient();
                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);
                    RequestParams params   = new RequestParams();

                    params.put("member_id", member_id);
                    params.put("amount", amount);
                    params.put("bank_code", bank_code);
                    params.put("product_code", product_code);
                    params.put("ccy_id", ccy_id);
                    params.put("sales_id", sales_id);
                    params.put("seller_fee", seller_fee);
                    params.put("buyer_fee", buyer_fee);
                    params.put("tx_fee", tx_fee);
                    params.put("commission_fee", commission_fee);

                    Log.d("params", params.toString());
                    client.post(AplConstants.ValidateCCLDIMobileAPI, params, new AsyncHttpResponseHandler() {
                        public void onSuccess(String content) {
                            Log.d("result:", content);
                            try {
                                JSONObject object         = new JSONObject(content);

                                String error_code         = object.getString("error_code");
                                String error_msg          = object.getString("error_message");

                                if (pDialog != null) {
                                    pDialog.dismiss();
                                }

                                if (error_code.equals(AppParams.SUCCESS_CODE)) {

                                    final String tx_fee_val          = object.getString("tx_fee");
                                    final String buyer_fee_val       = object.getString("buyer_fee");
                                    final String seller_fee_val      = object.getString("seller_fee");
                                    final String commission_fee_val  = object.getString("commission_fee");
                                    final String amount_data_val     = object.getString("amount");
                                    final String trx_id_val          = object.getString("trx_id");

                                    JSONArray array_invoice_val           = object.getJSONArray("invoice_data");
                                    final String invoice_data_val         = array_invoice_val.toString();

                                    Double dbl_amount  = Double.parseDouble(amount_data_val);
                                    int int_amount     = dbl_amount.intValue();

                                    String mStringArray[] = { Integer.toString(int_amount), sales_alias };
                                    JSONArray mJSONArray  = new JSONArray(Arrays.asList(mStringArray));
                                    data_inv              = mJSONArray.toString();

                                    pDialog = DefinedDialog.CreateProgressDialog(getActivity(), pDialog, "Meminta Data Token Payment Point...");
                                    AsyncHttpClient client = new AsyncHttpClient();
                                    client.setSSLSocketFactory(MySSLSocketFactory.getFixedSocketFactory());
                                    client.setTimeout(AplConstants.HTTP_DEFAULT_TIMEOUT);

                                    RequestParams params   = new RequestParams();
                                    params.put("data_inv", data_inv);
                                    params.put("member_id", member_id);
                                    params.put("bank_code", bank_code);
                                    params.put("product_code", product_code);
                                    params.put("ccy_id", ccy_id);
                                    params.put("shop_code", sales_name);
                                    params.put("shop_id", sales_id);
                                    params.put("no_phone_source", no_phone_source);
                                    params.put("seller_fee", seller_fee);
                                    params.put("buyer_fee", buyer_fee);
                                    params.put("tx_fee", tx_fee);
                                    params.put("benef_acct_no", benef_acct_no);
                                    params.put("benef_acct_name", benef_acct_name);
                                    params.put("charges_acct_no", charges_acct_no);
                                    params.put("fee_acct_no", fee_acct_no);
                                    params.put("commission_fee", commission_fee);
                                    params.put("commission_acct_no", commission_acct_no);
                                    params.put("resend_token", "N");

                                    Log.d("Debug param  ",params.toString());
                                    client.post(AplConstants.RequestTokenCCLDIMobileAPI, params, new AsyncHttpResponseHandler() {
                                        public void onSuccess(String content) {
                                            Log.d("result :", content);
                                            try {
                                                JSONObject object         = new JSONObject(content);

                                                String error_code         = object.getString("error_code");
                                                String error_msg          = object.getString("error_message");

                                                if (pDialog != null) {
                                                    pDialog.dismiss();
                                                }

                                                if (error_code.equals(AppParams.SUCCESS_CODE)) {
                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                    alert.setTitle("Payment Point");
                                                    alert.setMessage("Proses Meminta Token Payment Point Sukses");
                                                    alert.setPositiveButton("OK", null);
                                                    alert.show();

                                                    JSONArray arrDataReqToken = object.getJSONArray("data");
                                                    String rq_trx_id = arrDataReqToken.getJSONObject(0).getString("trx_id");
                                                    String rq_date   = arrDataReqToken.getJSONObject(0).getString("trx_date");
                                                    String rq_desc   = arrDataReqToken.getJSONObject(0).getString("desc");

                                                    Fragment newFragment = null;
                                                    newFragment = new FragmentConfirmTokenFifoHilo();
                                                    Bundle args = new Bundle();
                                                    args.putString("comm_id", comm_id);
                                                    args.putString("comm_name", comm_name);
                                                    args.putString("comm_code", comm_code);
                                                    args.putString("buss_scheme_code",buss_scheme_code);
                                                    args.putString("member_id", member_id);
                                                    args.putString("member_code", member_code);
                                                    args.putString("bank_code", bank_code);
                                                    args.putString("bank_name", bank_name);
                                                    args.putString("product_code", product_code);
                                                    args.putString("ccy_id", ccy_id);
                                                    args.putString("amount", amount);
                                                    args.putString("shop_code", sales_name);
                                                    args.putString("shop_id", sales_id);
                                                    args.putString("no_phone_source", no_phone_source);
                                                    args.putString("benef_acct_no", benef_acct_no);
                                                    args.putString("benef_acct_name", benef_acct_name);
                                                    args.putString("fee_acct_no", fee_acct_no);
                                                    args.putString("charges_acct_no", charges_acct_no);
                                                    args.putString("buyer_fee", buyer_fee);
                                                    args.putString("seller_fee", seller_fee);
                                                    args.putString("tx_fee", tx_fee);
                                                    args.putString("commission_fee", commission_fee);
                                                    args.putString("commission_acct_no", commission_acct_no);
                                                    args.putString("bbs_product_json", bbs_product);
                                                    args.putString("invoice_data_json", invoice_data_val);
                                                    args.putString("resend_token", "N");

                                                    args.putString("Rq_trx_id", rq_trx_id);
                                                    args.putString("Rq_date", rq_date);
                                                    args.putString("Rq_desc", rq_desc);

                                                    args.putString("sales_id", sales_id);
                                                    args.putString("sales_alias", sales_alias);
                                                    args.putString("sales_name", sales_name);
                                                    args.putString("flag_invoice", flag_invoice);
                                                    args.putString("hold_amount", hold_amount);
                                                    args.putString("bank_data", bank_data);

                                                    args.putString("amount_data_val", amount_data_val);
                                                    args.putString("tx_fee_val", tx_fee_val);
                                                    args.putString("buyer_fee_val", buyer_fee_val);
                                                    args.putString("seller_fee_val", seller_fee_val);
                                                    args.putString("commission_fee_val", commission_fee_val);


                                                    newFragment.setArguments(args);
                                                    switchFragment(newFragment);

                                                }else{
                                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                                    alert.setTitle("Payment Point");
                                                    alert.setMessage("Payment Point : " + error_msg);
                                                    alert.setPositiveButton("OK", null);
                                                    alert.show();
                                                }
                                            } catch (JSONException e) {
                                                // TODO Auto-generated catch block
                                                e.printStackTrace();
                                            }
                                        };

                                        public void onFailure(Throwable error, String content) {
                                            if (pDialog != null) {
                                                pDialog.dismiss();
                                            }
                                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                                        }

                                    });
                                } else {
                                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
                                    alert.setTitle("Payment Point");
                                    alert.setMessage("Payment Point : " + error_msg);
                                    alert.setPositiveButton("OK", null);
                                    alert.show();
                                }

                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        };

                        public void onFailure(Throwable error, String content) {
                            if (pDialog != null) {
                                pDialog.dismiss();
                            }
                            Toast.makeText(getActivity(), "Unexpected Error occurred! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });

        btnCancel               = (Button) view.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {

                Fragment newFragment = null;
                newFragment = new FragmentInputSalesAlias();
                Bundle args = new Bundle();
                args.putString("comm_id", comm_id);
                args.putString("comm_name", comm_name);
                args.putString("comm_code", comm_code);
                args.putString("buss_scheme_code",buss_scheme_code);
                args.putString("member_id", member_id);
                args.putString("member_code", member_code);
                newFragment.setArguments(args);
                switchFragment(newFragment);

            }
        });

        initViews();

        return view;
    }

    private void initViews() {
        try {
            JSONArray array  = new JSONArray(bank_data);

            int len = array.length();
            bank_code_arr = new String[len];
            bank_name_arr = new String[len];
            ccy_id_arr = new String[len];
            product_code_arr = new String[len];
            member_phone_arr = new String[len];
            benef_acct_no_arr = new String[len];
            benef_acct_name_arr = new String[len];
            tx_fee_arr = new String[len];
            fee_acct_no_arr = new String[len];
            seller_fee_arr = new String[len];
            charges_acct_no_arr = new String[len];
            buyer_fee_arr = new String[len];
            commission_acct_no_arr = new String[len];
            commission_fee_arr = new String[len];
            bbs_product_json_arr = new String[len];

            for (int i = 0; i < len; i++){
                bank_code_arr[i]          = array.getJSONObject(i).getString("bank_code");
                bank_name_arr[i]          = array.getJSONObject(i).getString("bank_name");
                ccy_id_arr[i]             = array.getJSONObject(i).getString("ccy_id");
                product_code_arr[i]       = array.getJSONObject(i).getString("product_code");
                member_phone_arr[i]       = array.getJSONObject(i).getString("member_phone");
                benef_acct_no_arr[i]      = array.getJSONObject(i).getString("benef_acct_no");
                benef_acct_name_arr[i]    = array.getJSONObject(i).getString("benef_acct_name");
                tx_fee_arr[i]             = array.getJSONObject(i).getString("tx_fee");
                fee_acct_no_arr[i]        = array.getJSONObject(i).getString("fee_acct_no");
                seller_fee_arr[i]         = array.getJSONObject(i).getString("seller_fee");
                charges_acct_no_arr[i]    = array.getJSONObject(i).getString("charges_acct_no");
                buyer_fee_arr[i]          = array.getJSONObject(i).getString("buyer_fee");
                commission_acct_no_arr[i] = array.getJSONObject(i).getString("commission_acct_no");
                commission_fee_arr[i]     = array.getJSONObject(i).getString("commission_fee");

                try{
                    JSONArray array_bbs       = array.getJSONObject(i).getJSONArray("bbs_product");
                    bbs_product_json_arr[i]   = array_bbs.toString();
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    bbs_product_json_arr[i]   = "";
                }

                BankList.add(new BankCCLBean(bank_code_arr[i], bank_name_arr[i], ccy_id_arr[i], product_code_arr[i], member_phone_arr[i], benef_acct_no_arr[i], benef_acct_name_arr[i], tx_fee_arr[i], fee_acct_no_arr[i], seller_fee_arr[i], charges_acct_no_arr[i], buyer_fee_arr[i], commission_acct_no_arr[i], commission_fee_arr[i], bbs_product_json_arr[i]));
            }
            BankCCLAdapter bankAdapter = new BankCCLAdapter(getActivity(), android.R.layout.simple_spinner_item, BankList);
            cbo_bank.setAdapter(bankAdapter);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            // TODO Auto-generated method stub
            super.handleMessage(msg);
        }

    };

    private void switchFragment(Fragment fragment) {
        if (getActivity() == null)
            return;
        MainActivity main = (MainActivity) getActivity();
        main.switchContent(fragment);
    }

}

