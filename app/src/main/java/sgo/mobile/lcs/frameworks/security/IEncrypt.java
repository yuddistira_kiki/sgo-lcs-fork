package sgo.mobile.lcs.frameworks.security;

public interface IEncrypt {
    public String encrypt(String str);
}
