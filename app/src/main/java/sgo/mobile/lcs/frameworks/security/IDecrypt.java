package sgo.mobile.lcs.frameworks.security;

public interface IDecrypt {
    public String decrypt(String str);
}
